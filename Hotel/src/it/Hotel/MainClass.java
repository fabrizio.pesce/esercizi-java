package it.Hotel;

public class MainClass {
    public static void main(String[] args) {
        Doppia cameraDoppia = new Doppia(1, false);
        Singola cameraSingola = new Singola(2, false);
        Suite cameraSuite = new Suite(3, false);

        GestorePrenotazioni gestorePrenotazioni = new GestorePrenotazioni();
        gestorePrenotazioni.aggiungiCamera(cameraDoppia);
        gestorePrenotazioni.aggiungiCamera(cameraSingola);
        gestorePrenotazioni.aggiungiCamera(cameraSuite);
        gestorePrenotazioni.visualizzaCamereDisponibili();
        gestorePrenotazioni.aggiornaStatoPrenotazione(5, true);
        gestorePrenotazioni.visualizzaCamereDisponibili();
        gestorePrenotazioni.rimuoviCamera(1);
        gestorePrenotazioni.visualizzaCamereDisponibili();
    }

}
