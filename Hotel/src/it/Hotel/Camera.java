package it.Hotel;

public abstract class Camera {
    private int numeroCamera;
    private boolean prenotata;

    public Camera(int numeroCamera, boolean prenotata) {
        this.numeroCamera = numeroCamera;
        this.prenotata = prenotata;
    }

    public int getNumeroCamera() {
        return numeroCamera;
    }

    public boolean isPrenotata() {
        return prenotata;
    }

    public void setNumeroCamera(int numeroCamera) {
        this.numeroCamera = numeroCamera;
    }

    public void setPrenotata(boolean prenotata) {
        this.prenotata = prenotata;
    }
}
