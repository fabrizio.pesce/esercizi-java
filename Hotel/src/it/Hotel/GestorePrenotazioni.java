package it.Hotel;

import java.util.HashMap;
import java.util.Map;

public class GestorePrenotazioni {
    private HashMap<Integer, Camera> mappaCamere;
    public GestorePrenotazioni(){
        this.mappaCamere = new HashMap<>();
    }

    public void aggiungiCamera(Camera camera){
        this.mappaCamere.put(camera.getNumeroCamera(), camera);
    }

    public void rimuoviCamera(int numeroCamera){
        this.mappaCamere.remove(numeroCamera);
    }

    public Camera cercaCamera(int numeroCamera){
        for(Map.Entry<Integer, Camera> entry : this.mappaCamere.entrySet()){
            if(entry.getKey() == numeroCamera){
                return entry.getValue();
            }
        }
        System.out.println("Camera non trovata!");
        return null;
    }
    public void aggiornaStatoPrenotazione(int numeroCamera, boolean prenotata){
        Camera cameraDaAggiornare = cercaCamera(numeroCamera);
        if(cameraDaAggiornare != null){
            cameraDaAggiornare.setPrenotata(prenotata);
            this.mappaCamere.put(numeroCamera, cameraDaAggiornare);
        }
    }

    public void visualizzaCamereDisponibili(){
        for(Map.Entry<Integer, Camera> entry : this.mappaCamere.entrySet()){
            if(!entry.getValue().isPrenotata()) {
                System.out.println("Numero camera: " + entry.getKey());
            }
        }
    }
}
