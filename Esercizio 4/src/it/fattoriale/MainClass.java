package it.fattoriale;

public class MainClass {
    public static void main(String[] args) {
        Factorial f1 = new Factorial(3);
        System.out.println(f1.getFactorial());
        f1.setFactorial(6);
        System.out.println(f1.getFactorial());
    }
}
