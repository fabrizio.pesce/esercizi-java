package it.fattoriale;
/*
* Scrivere un programma che calcoli il fattoriale di un numero assegnato n.
* Utilizzare una classe Fattoriale e una classe con il metodo main in cui verificare il funzionamento del fattoriale
* tramite una o più istanze della classe Fattoriale.
* */
public class Factorial {
    private int n;

    public Factorial(int n){
        this.n = factorial(n);
    }
    public int getFactorial() {
        return n;
    }

    public void setFactorial(int n) {
        this.n = factorial(n);
    }
    private int factorial(int n) {
        if(n<0){
            return 0;
        }
        if(n == 0) {
            return 1;
        }
        int returnValue = 1;
        for(int i = n; i>0; i--){
            returnValue *= i;
        }
        return returnValue;
    }
}
