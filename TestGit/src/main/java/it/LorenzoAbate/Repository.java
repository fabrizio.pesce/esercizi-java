package it.LorenzoAbate;

import java.util.List;

public class Repository {
    List<Commit> listaCommit;
    public List<Commit> getListaCommit() {
        return listaCommit;
    }
    public void setListaCommit(List<Commit> listaCommit) {
        this.listaCommit = listaCommit;
    }
    public Repository(List<Commit> listaCommit) {
        this.listaCommit = listaCommit;
    }
    public void aggiungiCommit(Commit commit){
        this.listaCommit.add(commit);
    }
    public Commit ultimoCommit(){
        if ( this.listaCommit == null || this.listaCommit.size() == 0 )
            return null;
        return this.listaCommit.get(this.listaCommit.size()-1);
    }
    public int numeroDiCommits(){
        return this.listaCommit.size();
    }
}
