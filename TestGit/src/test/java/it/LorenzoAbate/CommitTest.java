package it.LorenzoAbate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CommitTest {

    Commit commit;

    @BeforeEach
    void setUp(){
        commit = new Commit("1", "Primo Commit");
    }
    @Test
    void verificaCreazione(){
        assertNotNull(commit);
    }
}