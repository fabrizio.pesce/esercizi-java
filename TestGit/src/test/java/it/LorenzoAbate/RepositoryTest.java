package it.LorenzoAbate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class RepositoryTest {
    Repository repository;
    Commit commit1;
    Commit commit2;
    Commit commit3;

    @BeforeEach
    void setUp(){
        repository = new Repository(new ArrayList<>());
        commit1 = new Commit("1", "Primo commit");
        commit2 = new Commit("2", "Secondo commit");
        commit3 = new Commit("3", "Terzo commit");
    }
    @Test
    void testAggiunta(){
        assertNull(repository.ultimoCommit());
        repository.aggiungiCommit(commit1);
        assertEquals(1, repository.numeroDiCommits());
        repository.aggiungiCommit(commit2);
        assertEquals(2, repository.numeroDiCommits());
        repository.aggiungiCommit(commit3);
        assertEquals(3, repository.numeroDiCommits());
    }

    @Test
    void testUltimoCommit(){
        assertNull(repository.ultimoCommit());
        repository.aggiungiCommit(commit1);
        repository.aggiungiCommit(commit2);
        repository.aggiungiCommit(commit3);
        assertEquals(commit3, repository.ultimoCommit());
    }
    @Test
    void testContaCommit(){
        assertNull(repository.ultimoCommit());
        repository.aggiungiCommit(commit1);
        repository.aggiungiCommit(commit2);
        repository.aggiungiCommit(commit3);
        assertEquals(3, repository.numeroDiCommits());
        repository.getListaCommit().remove(2);
        assertEquals(2, repository.numeroDiCommits());
        repository.getListaCommit().remove(1);
        assertEquals(1, repository.numeroDiCommits());
        repository.getListaCommit().remove(0);
        assertEquals(0, repository.numeroDiCommits());
    }
}