package it.test;

public class First {
    public static void main(String[] args) {
        /*
        //Esercizio 1
        int a = 5;
        double b = 13.5;
        byte c = 8; // Operazioni aritmetiche
        int risultato1 = a + (int)b;
        double risultato2 = b / c;
        byte risultato3 = (byte)(a * c);
        //Esercizio 2
        short x = 120;
        int y = 40000;
        float z = 3.14f;
        long w = 123456789L;
// Operazioni complesse
        float risultato1 = (float)y / x + z; //324,8335228
        long risultato2 = w / (y / (x - 2)); //
        int risultato3 = (int)((double)x * z - (double)y / w); // 376 = (376,8‬ - 0,000324000002948)
// Cast espliciti
        byte risultato4 = (byte)(x * 2); //overflow oppure 127
        double risultato5 = (double)(w - y) * z; //(double)387.528.717,46‬
        */
        //Esercizio 3
        int m = 20; // 21 20
        double n = -45.55; //-45.55 -44.55 -45.55
        char o = 'A'; //"valore prima di A" "A" "B"
        long p = 15000000000L;
        // Operazioni con operatori unari
        int risultato1 = ++m * 3; //63
        double risultato2 = n++ + --m; //25.55
        int risultato3 = o--; //A
        long risultato4 = -p; //-15000000000
        // Operazioni miste con cast
        double risultato5 = (double)(++o * m) / p; // 8.666666666666666E-8 = 1300.0/15000000000L
        int risultato6 = (int)(--n) / m; //-2
        long risultato7 = p % ++o; //18
        System.out.println(risultato1+" "+risultato2+" "+risultato3+" "+risultato4+" "+risultato5+" "+risultato6+" "+risultato7);
    }
}
