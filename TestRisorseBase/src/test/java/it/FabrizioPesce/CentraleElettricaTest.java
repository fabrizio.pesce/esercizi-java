package it.FabrizioPesce;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Classe che testa Centrale Elettrica
 */
class CentraleElettricaTest {

    CentraleElettrica centraleElettrica;
    RisorsaEnergetica risorsaEnergetica1;
    RisorsaEnergetica risorsaEnergetica2;

    @BeforeEach
    void setUp(){
        centraleElettrica = new CentraleElettrica(1, 10);
        risorsaEnergetica1 = new RisorsaEnergetica(RisorsaEnergetica.NomeRisorsa.Metano, 6, 4, 10);
        risorsaEnergetica2 = new RisorsaEnergetica(RisorsaEnergetica.NomeRisorsa.Metano, 6, 4, 10);

        centraleElettrica.aggiungiRisorsa(risorsaEnergetica1);
        centraleElettrica.aggiungiRisorsa(risorsaEnergetica2);
    }

    /**
     * Metodo che testa se la variabile centrale elettrica viene istanziata correttamente
     */
    @Test
    @Order(1)
    void testCentraleElettrica(){
        assertNotNull(centraleElettrica);
    }

    /**
     * Metodo che testa se le variabili risorse energetiche vengono istanziate correttamente
     */
    @Test
    @Order(2)
    void testRisorsaEnergetica(){
        assertNotNull(risorsaEnergetica1);
        assertNotNull(risorsaEnergetica2);
    }

    /**
     * Metodo che testa la funzione relativa al calcolo del costo dell'energia dato in input un consumo
     */
    @Test
    @Order(3)
    void testCalcolaCostoEnergia(){
        assertEquals(10,centraleElettrica.calcolaCostoEnergia(1));
    }

    /**
     * Metodo che calcola la produzione energetica della centrale
     */
    @Test
    @Order(4)
    void testCalcolaProduzioneEnergetica(){
        assertEquals(80,centraleElettrica.calcolaProduzioneEnergetica());
    }

    /**
     * Metodo che testa la simulazione di consumo e mi permette di visualizzare le risorse rimaste
     */
    @Test
    @Order(5)
    void testSimulazione(){
        centraleElettrica.simulaConsumo(50);
        centraleElettrica.visualizzaRisorse();
    }

}