package it.FabrizioPesce;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe che rappresenta una centrale elettrica
 */
public class CentraleElettrica {
    private List<RisorsaEnergetica> risorseDisponibili;
    private int numRisorse;
    private double efficienza;

    /**
     * Costruttore della classe centrale elettrica, per memorizzare le risorse disponibili utilizzo un'array list
     * @param numRisorse int
     * @param efficienza double
     */
    public CentraleElettrica(int numRisorse, double efficienza) {
        this.risorseDisponibili = new ArrayList<>();
        this.numRisorse = numRisorse;
        this.efficienza = efficienza;
    }

    /**
     * Metodo che aggiunge una risorsa energetica alla centrale
     * @param risorsa RisorsaEnergetica
     */
    public void aggiungiRisorsa(RisorsaEnergetica risorsa){
        this.risorseDisponibili.add(risorsa);
        numRisorse++;
    }

    /**
     * Metodo che mi permette di visualizzare le risorse disponibili alla centrale
     */
    public void visualizzaRisorse(){
        for(RisorsaEnergetica risorsa : this.risorseDisponibili){
            if(risorsa.getQuantitaDisponibile()>0){
                System.out.println("Risorsa: "+risorsa.getNome()+
                        "\nQuantità: "+risorsa.getQuantitaDisponibile()+
                        "\nPotenza Calorifica: "+risorsa.getPotenzaCalorifica()+
                        "\nPrezzo: "+risorsa.getPrezzo());
            }
        }
    }

    /**
     * Funzione che calcola la produzione energetica
     * @return double
     */
    public double calcolaProduzioneEnergetica(){
        double total = 0.0;
        for(RisorsaEnergetica risorsa : this.risorseDisponibili){
            if(risorsa.getQuantitaDisponibile()>0){
                total += (risorsa.getPotenzaCalorifica()*this.efficienza);
            }
        }
        return total;
    }

    /**
     * Metodo che simula il consumo della centrale.
     * Ho provato a pensarla meglio che potevo, non sono bravo in fisica.
     * @param consumo double
     */
    public void simulaConsumo(double consumo){
        double quantita = 0;

        for(RisorsaEnergetica risorsa : this.risorseDisponibili){
            double quantitaRisorsa = 0;
            while(risorsa.getQuantitaDisponibile()>0){
                quantita += risorsa.getPotenzaCalorifica();
                if( quantita >= consumo){
                    quantitaRisorsa++;
                    risorsa.setQuantitaDisponibile(risorsa.getQuantitaDisponibile()-1);
                    break;
                }
                risorsa.setQuantitaDisponibile(risorsa.getQuantitaDisponibile()-1);
                if(risorsa.getQuantitaDisponibile() == 0){
                    numRisorse--;
                }
                quantitaRisorsa++;
            }
            System.out.println("Utilizzo: "+ risorsa.getNome()+" in quantità: "+quantitaRisorsa);
        }
    }

    /**
     * Metodo che calcola il costo per produrre una certa quantità di energia.
     * @param consumo double
     * @return double
     */
    public double calcolaCostoEnergia(double consumo){
        double total = 0;
        double quantita = 0;
        double quantitaUtilizzata = 0;
        for(RisorsaEnergetica risorsa : this.risorseDisponibili){
            quantitaUtilizzata = risorsa.getQuantitaDisponibile()-1;
            while(quantitaUtilizzata > 0){
                quantita += risorsa.getPotenzaCalorifica();
                if( quantita >= consumo){
                    total += (risorsa.calcolaCosto(risorsa.getQuantitaDisponibile()-quantitaUtilizzata));
                    return total;
                }
                total += (risorsa.calcolaCosto(risorsa.getQuantitaDisponibile()));
                quantitaUtilizzata--;
            }
        }
        return total;
    }
}
