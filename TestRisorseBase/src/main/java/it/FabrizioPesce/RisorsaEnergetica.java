package it.FabrizioPesce;

/**
 * Classe che rappresenta la risorsa energetica.
 */
public class RisorsaEnergetica {
    public enum NomeRisorsa{
        Metano,
        Carbone
    }
    private NomeRisorsa nome;
    private double quantitaDisponibile;
    private double potenzaCalorifica;
    private double prezzo;

    /**
     * Costruttore della classe RisorsaEnergetica
     * @param nome NomeRisorsa
     * @param quantitaDisponibile double
     * @param potenzaCalorifica double
     * @param prezzo double
     */
    public RisorsaEnergetica(NomeRisorsa nome, double quantitaDisponibile, double potenzaCalorifica, double prezzo) {
        this.nome = nome;
        this.quantitaDisponibile = quantitaDisponibile;
        this.potenzaCalorifica = potenzaCalorifica;
        this.prezzo = prezzo;
    }

    /**
     * Metodo che mi ritorna il nome della risorsa
     * @return NomeRisorsa
     */
    public NomeRisorsa getNome() {
        return nome;
    }

    /**
     * Metodo che mi ritorna la quantita disponibile della risorsa
     * @return double
     */
    public double getQuantitaDisponibile() {
        return quantitaDisponibile;
    }

    /**
     * Meotod che mi ritorna la potenza calorifica della risorsa
     * @return double
     */
    public double getPotenzaCalorifica() {
        return potenzaCalorifica;
    }

    /**
     * Metodo che mi ritorna il prezzo della risorsa(€)
     * @return double
     */
    public double getPrezzo() {
        return prezzo;
    }

    /**
     * Metodo che imposta la quantità disponibile della risorsa
     * @param quantitaDisponibile double
     */
    public void setQuantitaDisponibile(double quantitaDisponibile) {
        this.quantitaDisponibile = quantitaDisponibile;
    }

    /**
     * Metodo che calcola il costo della risorsa data la quantità.
     * @param quantita double
     * @return double
     */
    public double calcolaCosto(double quantita){
        return this.prezzo*quantita;
    }
}
