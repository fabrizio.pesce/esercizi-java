import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

import static org.apache.logging.log4j.EventLogger.logEvent;


public class MainClass {
    private static final String DB_URL = "jdbc:mysql://localhost:3306/GITTEST";
    private static final String DB_USER = "root";
    private static final String DB_PASSWORD = "rootroot";

    private static final Logger logger = (Logger) LogManager.getLogger(MainClass.class);

    public static void main(String[] args) throws IOException, InterruptedException {
        String dir = System.getProperty("user.home")+"\\OneDrive - NTT DATA EMEAL\\Desktop\\test\\git-test";
        String fileToCheck = "ciao.txt";
        try {
            monitor(dir, fileToCheck);
        } catch (NoSuchAlgorithmException e) {
            logger.error("Errore durante il monitoraggio delle modifiche: " + e.getMessage());
        }
    }
    public static void monitor(String dirPath, String fileToCheck) throws NullPointerException, NoSuchAlgorithmException, IOException {

        File dir = new File(dirPath);
        HashMap<String, Long> fileInDir = new HashMap<>();
        File[] arrayOfFile;
        boolean doAction = false;
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        String previousHash = "";

        while(true){

            String currentHash = calculateHashOfFile(dirPath + File.separator + fileToCheck, digest);
            //Aggiorno la lista di file
            arrayOfFile = dir.listFiles();

            try{
                //Aggiungo i nuovi file della cartella in una lista di file
                for(File file : arrayOfFile){

                    if(fileInDir.get(file.getName()) == null ||  !currentHash.equals(previousHash)){
                        fileInDir.put(file.getName(), file.lastModified());
                        doAction = true;
                        System.out.println("File aggiornato trovato");
                        if(file.getName().equals(fileToCheck)){
                            previousHash = currentHash;
                            System.out.println("Il tuo file è stato aggiornato");
                            MainClass.logEvent("File modificato", file.getPath());
                        }
                    }
                }
                //Se alcuni dei file aggiunti non sono più presenti nella cartella li rimuovo dalla lista
                Iterator<Map.Entry<String, Long>> iterator = fileInDir.entrySet().iterator();
                while (iterator.hasNext()) {
                    Map.Entry<String, Long> entry = iterator.next();
                    String fileName = entry.getKey();
                    File file = new File(dirPath + File.separator + fileName);
                    if (!file.exists()) {
                        iterator.remove();
                        doAction = true;
                    }
                }

                if(doAction){
                    launchCommand(dirPath);
                    doAction = false;
                }

            }catch (NullPointerException nullPointerException){
                System.out.println("La cartella non esiste");
                throw new NullPointerException();
            }catch(SQLException sqlException){
                System.out.println("Problemi nella connessione al db");
            }

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private static void logEvent(String description,String filePath) throws SQLException {
        logger.info(description + ": " + filePath);
        insertIntoDatabase(description, filePath);
    }

    public static void launchCommand(String dirPath) {
        try {
            commandGitAdd(dirPath);
            commandGitCommit(dirPath);
        } catch(ExecuteException executeException){
            if(executeException.getExitValue() == 1){
                System.out.println("Non ci sono aggiornamenti da committare. Tutto in regola qui!");
            }else{
                System.out.println("Ops. Credo ci sia stato un errore.");
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try {
            commandGitPush(dirPath);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void commandGitAdd(String dirPath) throws IOException {
        CommandLine cmd = CommandLine.parse("git");
        cmd.addArgument("add");
        cmd.addArgument(".");
        DefaultExecutor executor = DefaultExecutor.builder().get();
        executor.setWorkingDirectory(new File(dirPath));
        executor.setStreamHandler(new PumpStreamHandler(null, null, null));
        int exitValue = executor.execute(cmd);
        if (exitValue == 0) {
            System.out.println("Il comando git add . è stato eseguito con successo.");
        }else {
            System.out.println("Si è verificato un errore durante l'esecuzione del comando git add .");
        }
    }

    private static void commandGitCommit(String dirPath) throws IOException {
        CommandLine cmd = CommandLine.parse("git");
        cmd.addArgument("commit");
        cmd.addArgument("-m");
        cmd.addArgument("'Aggiornamento automatico'");
        DefaultExecutor executor = DefaultExecutor.builder().get();
        executor.setWorkingDirectory(new File(dirPath));
        executor.setStreamHandler(new PumpStreamHandler(null, null, null));
        int exitValue = executor.execute(cmd);
        if (exitValue == 0) {
            System.out.println("Il comando commit è stato eseguito con successo.");
        } else {
            System.out.println("Si è verificato un errore durante l'esecuzione del comando git commit");
        }
    }

    private static void commandGitPush(String dirPath) throws IOException {
        CommandLine cmd = CommandLine.parse("git");
        cmd.addArgument("push");
        DefaultExecutor executor = DefaultExecutor.builder().get();
        executor.setWorkingDirectory(new File(dirPath));
        executor.setStreamHandler(new PumpStreamHandler(null, null, null));
        int exitValue = executor.execute(cmd);
        if (exitValue == 0) {
            System.out.println("Il comando push è stato eseguito con successo.");
        } else {
            System.out.println("Si è verificato un errore durante l'esecuzione del comando git push");
        }
    }

    public static String calculateHashOfFile(String filePath, MessageDigest digest) throws IOException {
        File file = new File(filePath);
        try (FileInputStream fis = new FileInputStream(file)) {
            byte[] buffer = new byte[8192]; // Usiamo un buffer di 8KB
            int bytesRead;
            while ((bytesRead = fis.read(buffer)) != -1) {
                digest.update(buffer, 0, bytesRead);
            }
        }
        byte[] hashBytes = digest.digest();
        StringBuilder hexHash = new StringBuilder();
        for (byte b : hashBytes) {
            hexHash.append(String.format("%02x", b));
        }
        return hexHash.toString();
    }

    public static void insertIntoDatabase(String description, String filePath) throws SQLException {
        try (Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD)) {
            String sql = "INSERT INTO EVENT (description, filePath) VALUES (?, ?)";
            try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
                pstmt.setString(1, description);
                pstmt.setString(2, filePath);
                pstmt.executeUpdate();
            }
        }
    }
}
