package it.hieracy;

public class Sphere extends generic3D{

    public Sphere(int genericLength) {
        super(genericLength);
    }

    @Override
    public float volume() {
        return (float) (Math.pow(this.getGenericLength(),3)*Math.PI*((double) 4 /3));
    }
}
