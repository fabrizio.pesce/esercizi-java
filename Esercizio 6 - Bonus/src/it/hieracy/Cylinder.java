package it.hieracy;

public class Cylinder extends generic3D{
    private int height;
    public Cylinder(int genericLength, int height) {
        super(genericLength);
        this.height = height;
    }
    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
    @Override
    public float volume() {
        return (float) (Math.pow(this.getGenericLength(),2)*Math.PI*height);
    }

}
