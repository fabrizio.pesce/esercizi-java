package it.hieracy;

public abstract class generic3D {
    private int genericLength;

    public generic3D(int genericLength){
        this.genericLength = genericLength;
    }

    public void setGenericLength(int genericLength) {
        this.genericLength = genericLength;
    }

    public int getGenericLength() {
        return genericLength;
    }

    public abstract float volume();
    public void printVolume(){
        System.out.println("Il volume della tua figura è: "+volume());
    }
}
