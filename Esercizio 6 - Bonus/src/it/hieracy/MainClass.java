package it.hieracy;

public class MainClass {
    public static void main(String[] args) {
        Cube cube = new Cube(7);
        cube.printVolume();
        Sphere sphere = new Sphere(7);
        sphere.printVolume();
        Cylinder cylinder = new Cylinder(7, 3);
        cylinder.printVolume();
    }
}
