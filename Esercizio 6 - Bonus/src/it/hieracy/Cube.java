package it.hieracy;

public class Cube extends generic3D{
    public Cube(int genericLength) {
        super(genericLength);
    }

    @Override
    public float volume() {
        return (float) Math.pow(this.getGenericLength(),3);
    }
}
