import java.io.*;

public class MainClass {
    public static void main(String[] args) {
        String fileInputPath = "src\\input.txt";
        String fileOutputPath = "src\\output.txt";
        File file = new File(fileInputPath);
        try {
            FileReader fr = new FileReader(fileInputPath);
            int i = 0;
            String number = "";
            int total = 0;
            String outputString = "";
            while(i != -1){
                i = fr.read();
                switch((char) i){
                    case '\n':
                        total += Integer.parseInt(number);
                        outputString += total+", ";
                        number = "";
                        total = 0;
                        break;
                    case ',':
                        total += Integer.parseInt(number);
                        number = "";
                        break;
                    case '\r':
                        break;
                    case '\uFFFF':
                        total += Integer.parseInt(number);
                        outputString += total;
                        System.out.println(outputString);
                        total = 0;
                        break;
                    default:
                        number += (char)i;
                        break;
                }
            }

            FileWriter writer = new FileWriter(fileOutputPath);
            writer.write(outputString);
            writer.close();

        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
