package it.FilesException;

import java.util.Scanner;

public class MainClass {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        FileSorgente fileSorgente = new FileSorgente("FirstTestException", File.TipoFile.Java, null);
        fileSorgente.aggiungiTesto(input.nextLine());
        System.out.println(fileSorgente.getContenuto());
        try{
            fileSorgente.aggiungiTesto(input.nextLine(), input.nextInt());
            System.out.println(fileSorgente.getContenuto());
        }catch(IllegalArgumentException illegalArgumentException){
            System.out.println(illegalArgumentException.getMessage());
        }catch(StringaVuota stringaVuota){
            System.out.println(stringaVuota.getMessage());
        }catch(Exception e){
            System.out.println("Eccezione generica");
        }
    }
}
