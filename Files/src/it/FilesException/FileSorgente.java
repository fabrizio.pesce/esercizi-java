package it.FilesException;

public class FileSorgente extends File{
    private String contenuto;
    public FileSorgente(String nome, TipoFile tipo, String contenuto) {
        super(nome, tipo);
        this.contenuto = contenuto;
    }

    public void setContenuto(String contenuto) {
        this.contenuto = contenuto;
    }

    public String getContenuto() {
        return contenuto;
    }

    public void aggiungiTesto(String testoDaAggiungere){
        this.contenuto += testoDaAggiungere;
    }

    public void aggiungiTesto(String testoDaAggiungere, int posizione) throws StringaVuota {
        if(posizione >= this.contenuto.length()){
            throw new IllegalArgumentException("Posizione fuori dal contenuto");
        }
        if(this.contenuto.isEmpty()){
            throw new StringaVuota("Stringa vuota");
        }
        String stringAfterIndex = this.contenuto.substring(posizione-1);
        String stringBeforeIndex = this.contenuto.substring(0,posizione-1);
        this.contenuto = stringBeforeIndex + testoDaAggiungere + stringAfterIndex;
    }
}
