package it.FilesException;

public class StringaVuota extends Exception{
    public StringaVuota() {
    }

    public StringaVuota(String message) {
        super(message);
    }
}
