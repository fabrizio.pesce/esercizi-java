package it.FilesException;

public abstract class File{

    public enum TipoFile{
        Java,
        CSharp,
        PHP,
        CSS,
        HTML,
        JavaScript
    }

    private String nome;
    private TipoFile tipo;

    public File(String nome, File.TipoFile tipo) {
        this.nome = nome;
        this.tipo = tipo;
    }

}
