package it.Negozio;

public class MainClass {
    public static void main(String[] args) {
        Prodotto prodotto1 = new Prodotto(1, new Scarpe(), "Nike", 149.99, 10);
        Cliente cliente1 = new Cliente(1, "Fabrizio", 20);
        Ordine ordine1 = new Ordine(cliente1, prodotto1, 1);
        NegozioAbbigliamento negozioAbbigliamento = new NegozioAbbigliamento();
        negozioAbbigliamento.aggiungiProdotto(prodotto1);
        negozioAbbigliamento.visualizzaProdottiDisponibili();
        negozioAbbigliamento.aggiungiCliente(cliente1);
        negozioAbbigliamento.effettuaOrdine(ordine1);
        negozioAbbigliamento.visualizzaOrdini();
    }
}
