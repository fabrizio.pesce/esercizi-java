package it.Negozio;

import java.util.HashMap;
import java.util.Map;

public class NegozioAbbigliamento {
    private HashMap<Integer, Prodotto> elencoProdotti;
    private HashMap<Integer, Cliente> elencoClienti;
    private HashMap<Integer, Ordine> elencoOrdini;
    int idOrdine;

    public NegozioAbbigliamento(){
        this.elencoProdotti = new HashMap<>();
        this.elencoClienti = new HashMap<>();
        this.elencoOrdini = new HashMap<>();
        idOrdine = 0;
    }

    public void aggiungiProdotto(Prodotto prodotto){
        this.elencoProdotti.put(prodotto.getCodiceProdotto(), prodotto);
    }
    public void rimuoviProdotto(int codiceProdotto){
        this.elencoProdotti.remove(codiceProdotto);
    }

    public void aggiungiCliente(Cliente cliente){
        this.elencoClienti.put(cliente.getCodiceCliente(), cliente);
    }
    public void rimuoviCliente(int codiceCliente){
        this.elencoClienti.remove(codiceCliente);
    }
    public void effettuaOrdine(Ordine ordine){
        this.elencoOrdini.put(idOrdine++, ordine);
    }

    public void visualizzaProdottiDisponibili(){
        for(Map.Entry<Integer, Prodotto> entry : this.elencoProdotti.entrySet()){
            System.out.println("Prodotto "+entry.getKey()+": "+entry.getValue().getDescrizione());
        }
    }

    public void visualizzaClienti(){
        for(Map.Entry<Integer, Cliente> entry : this.elencoClienti.entrySet()){
            System.out.println("Cliente "+entry.getKey()+": "+entry.getValue().getNomeCliente());
        }
    }

    public void visualizzaOrdini(){
        for(Map.Entry<Integer, Ordine> entry : this.elencoOrdini.entrySet()){
            System.out.println("Ordine "+entry.getKey()
                    +": "+entry.getValue().getCliente().getNomeCliente()
                    +" "+entry.getValue().getProdotto().getDescrizione()
                    +" "+entry.getValue().getQuantita());
        }
    }
}
