package it.Negozio;

public class Cliente {
    private int codiceCliente;
    private String nomeCliente;
    private int etaCliente;

    public Cliente(int codiceCliente, String nomeCliente, int etaCliente) {
        this.codiceCliente = codiceCliente;
        this.nomeCliente = nomeCliente;
        this.etaCliente = etaCliente;
    }

    public int getCodiceCliente() {
        return codiceCliente;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public int getEtaCliente() {
        return etaCliente;
    }

    public void setCodiceCliente(int codiceCliente) {
        this.codiceCliente = codiceCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    public void setEtaCliente(int etaCliente) {
        this.etaCliente = etaCliente;
    }
}
