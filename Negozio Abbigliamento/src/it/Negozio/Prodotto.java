package it.Negozio;

public class Prodotto {
    private int codiceProdotto;
    private TipoProdotto tipoProdotto;
    private String descrizione;
    private double prezzo;
    private int quantitaDisponibile;

    public Prodotto(int codiceProdotto, TipoProdotto tipoProdotto, String descrizione, double prezzo, int quantitaDisponibile) {
        this.codiceProdotto = codiceProdotto;
        this.tipoProdotto = tipoProdotto;
        this.descrizione = descrizione;
        this.prezzo = prezzo;
        this.quantitaDisponibile = quantitaDisponibile;
    }

    public int getCodiceProdotto() {
        return codiceProdotto;
    }

    public TipoProdotto getTipoProdotto() {
        return tipoProdotto;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public double getPrezzo() {
        return prezzo;
    }

    public int getQuantitaDisponibile() {
        return quantitaDisponibile;
    }

    public void setCodiceProdotto(int codiceProdotto) {
        this.codiceProdotto = codiceProdotto;
    }

    public void setTipoProdotto(TipoProdotto tipoProdotto) {
        this.tipoProdotto = tipoProdotto;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public void setPrezzo(double prezzo) {
        this.prezzo = prezzo;
    }

    public void setQuantitaDisponibile(int quantitaDisponibile) {
        this.quantitaDisponibile = quantitaDisponibile;
    }
}
