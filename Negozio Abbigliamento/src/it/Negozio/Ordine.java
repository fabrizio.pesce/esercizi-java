package it.Negozio;

import java.util.HashMap;
import java.util.Map;

public class Ordine {

    private Cliente cliente;
    private Prodotto prodotto;
    private int quantita;

    public Ordine(Cliente cliente, Prodotto prodotto, int quantita) {
        this.cliente = cliente;
        this.prodotto = prodotto;
        this.quantita = quantita;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public Prodotto getProdotto() {
        return prodotto;
    }

    public int getQuantita() {
        return quantita;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public void setProdotto(Prodotto prodotto) {
        this.prodotto = prodotto;
    }

    public void setQuantita(int quantita) {
        this.quantita = quantita;
    }
}
