package it.azienda;

public class Impiegato extends Dipendente{
    private String reparto;
    public Impiegato(String nome, double stipendio, String reparto) {
        super(nome, stipendio);
        this.reparto = reparto;
    }

    @Override
    public void stampaInfo() {
        super.stampaInfo();
        System.out.println("Reparto: "+this.reparto);
    }
}
