package it.azienda;

public class Dipendente {
    private String nome;
    private double stipendio;

    public Dipendente(String nome, double stipendio) {
        this.nome = nome;
        this.stipendio = stipendio;
    }

    public void stampaInfo(){
        System.out.println("Nome: "+this.nome
                +"\nStipendio: "+this.stipendio);
    }
}
