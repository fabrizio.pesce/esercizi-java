package it.azienda;

public class MainClass {
    public static void main(String[] args) {
        Impiegato i = new Impiegato("Pablo", 1300.00, "Logistica");
        Manager m = new Manager("Gustav", 2500.50, "HR");

        i.stampaInfo();
        m.stampaInfo();
    }
}
