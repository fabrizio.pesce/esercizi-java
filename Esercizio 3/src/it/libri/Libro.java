package it.libri;

public class Libro {

    String titolo;
    String ISBN;
    String nomeAutore;
    int annoUscita;

    /**
     * Costruttore vuoto
     * @author Fabrizio Pesce
     */
    public Libro(){}
    /**
     * Costruttore con solo ISBN
     * @author Fabrizio Pesce
     * @param ISBN String
     */
    public Libro(String ISBN){
        this.ISBN = ISBN;
    }

    /**
     * Costruttore con titolo e ISBN
     * @author Fabrizio Pesce
     * @param titolo String
     * @param ISBN String
     */
    public Libro(String titolo, String ISBN) {
        this.titolo = titolo;
        this.ISBN = ISBN;
    }

    /**
     * Costruttore completo
     * @author Fabrizio Pesce
     * @param titolo String
     * @param ISBN String
     * @param nomeAutore String
     * @param annoUscita int
     */
    public Libro(String titolo, String ISBN, String nomeAutore, int annoUscita) {
        this.titolo = titolo;
        this.ISBN = ISBN;
        this.nomeAutore = nomeAutore;
        this.annoUscita = annoUscita;
    }

    /**
     * Metodo che restituisce il titolo
     * @return String
     */
    public String getTitolo() {
        return this.titolo;
    }
    /**
     * Metodo che restituisce l'ISBN
     * @return String
     */
    public String getISBN() {
        return this.ISBN;
    }
    /**
     * Metodo che restituisce il nome dell'autore
     * @return String
     */
    public String getNomeAutore() {
        return this.nomeAutore;
    }
    /**
     * Metodo che restituisce l'anno di uscita
     * @return int
     */
    public int getAnnoUscita() {
        return this.annoUscita;
    }
    /**
     * Metodo che imposta il titolo
     */
    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }
    /**
     * Metodo che imposta l'ISBN
     */
    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }
    /**
     * Metodo che imposta il nome dell'autore
     */
    public void setNomeAutore(String nomeAutore) {
        this.nomeAutore = nomeAutore;
    }
    /**
     * Metodo che imposta l'anno di uscita
     */
    public void setAnnoUscita(int annoUscita) {
        this.annoUscita = annoUscita;
    }
    /**
     * Metodo che stampa nella console le informazione del libro
     */
    public void stampaInfo(){
        System.out.println("Titolo: "+this.titolo
                +"\nISBN: "+this.ISBN
                +"\nNome autore: "+this.nomeAutore
                +"\nAnno di uscita: "+this.annoUscita);
    }
    /**
     * Metodo che ritorna le informazioni del libro, vengono suddivise da un -
     * @return String
     */
    public String ottieniInfo(){
        return this.titolo
                +"-"+this.ISBN
                +"-"+this.nomeAutore
                +"-"+this.annoUscita;
    }
}
