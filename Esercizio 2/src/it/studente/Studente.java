package it.studente;

public class Studente {
    private String nome;
    private String cognome;
    private String matricola;

    /**
     * Classe studente
     * @author Fabrizio Pesce
     * @param nome private String
     * @param cognome private String
     * @param matricola private String
     */
    public Studente(String nome, String cognome, String matricola){
        this.nome = nome;
        this.cognome = cognome;
        this.matricola = matricola;
    }

    /**
     * Restituisce il nome dello studente
     * @return String
     */
    public String getNome(){
        return this.nome;
    }

    /**
     * Restituisce il cognome dello studente
     * @return String
     */
    public String getCognome(){
        return this.cognome;
    }
    /**
     * Restituisce la matricola dello studente
     * @return String
     */
    public String getMatricola(){
        return this.matricola;
    }
    /**
     * Imposta il nome dello studente
     */
    public void setNome(String nome){
        this.nome = nome;
    }
    /**
     * Imposta il cognome dello studente
     */
    public void setCognome(String cognome){
        this.cognome = cognome;
    }
    /**
     * Imposta la matricola dello studente
     */
    public void setMatricola(String matricola){
        this.matricola = matricola;
    }
}
