package it.studente;

public class MainClass {
    public static void main(String[] args) {
        Studente studente1 = new Studente("Andrea", "", "");
        Studente studente2 = new Studente("Fabrizio", "", "186398");
        System.out.println("Studente 1: "+studente1.getNome()+" "+studente1.getCognome()+
                ", "+studente1.getMatricola());
        System.out.println("Studente 2: "+studente2.getNome()+" "+studente2.getCognome()+
                ", "+studente2.getMatricola());
        System.out.println("Ora completiamo i campi mancanti :D");
        studente1.setCognome("Rossi");
        studente1.setMatricola("186397");
        studente2.setCognome("Pesce");
        System.out.println("Ecco fatto!");
        System.out.println("Studente 1: "+studente1.getNome()+" "+studente1.getCognome()+
                ", "+studente1.getMatricola());
        System.out.println("Studente 2: "+studente2.getNome()+" "+studente2.getCognome()+
                ", "+studente2.getMatricola());
    }
}
