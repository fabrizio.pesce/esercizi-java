package it.NumbersException;

import java.util.Scanner;

public class MainClass {
    public static void main(String[] args) {
        int[] array = new int[10];
        checkFormatOfImaginaryNumber("5i2");
    }

    public static void checkFormatOfImaginaryNumber(String number){
        try{
            if(number.contains("i")) {
                String subStringNumber = number.substring(1);
                Double.parseDouble(subStringNumber);
                System.out.println(number+" è un numero immaginario :D");
            }else{
                Double.parseDouble(number);
                System.out.println("è un numero reale :D");
            }
        }catch(NumberFormatException formatException){
            System.out.println("Hai passato una stringa formattata male :(\n"+formatException.getMessage());
        }
    }
}
