package it.azienda;

public abstract class Dipendente {
    private String nome;
    private int codice;

    public Dipendente(String nome, int codice){
        this.   nome = nome;
        this.codice = codice;
    }

    public String getNome() {
        return nome;
    }

    public int getCodice() {
        return codice;
    }

    public abstract double calcolaStipendio();

}
