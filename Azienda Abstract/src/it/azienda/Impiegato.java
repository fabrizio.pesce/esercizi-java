package it.azienda;

public class Impiegato extends Dipendente{
    private double stipendioBase;

    public double getStipendioBase() {
        return stipendioBase;
    }

    public Impiegato(String nome, int codice, double stipendioBase) {
        super(nome, codice);
        this.stipendioBase = stipendioBase;
    }

    @Override
    public double calcolaStipendio() {
        return (stipendioBase/100)*110;
    }
}
