package it.azienda;

public class MainClass {
    public static void main(String[] args) {
        Impiegato i = new Impiegato("Pablo", 1300, 1500);

        System.out.println("Nome: "+i.getNome()
                +"\nCodice: "+i.getCodice()
                +"\nStipendio base: "+i.getStipendioBase()
                +"\nStipendio calcolato: "+i.calcolaStipendio());
    }
}
