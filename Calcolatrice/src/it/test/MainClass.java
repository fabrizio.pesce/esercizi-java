package it.test;

import java.util.Scanner;

public class MainClass {
    public static void main(String[] args) {
        boolean exitCondition = false;
        int bracketTotal = 0;
        boolean bracket = false;
        String valueInput;
        Scanner input = new Scanner(System.in);

        System.out.println("Benvenuto nella calcolatrice." +
                "\nScrivi . per uscire altrimenti inizia a digitare!\nAttenzione, potrai inserire fino a 3 parentesi!!");
        int[] total = {0, 0, 0, 0};
        String op = "";
        int depth = 0;

        while (!(valueInput = getValueFromUser()).equals(".")) {
            int val;
            if (valueInput.equals("(")) {
                depth++;
                System.out.println("Parentesi n." + depth);
            } else if (valueInput.equals(")")) {
                depth--;
                System.out.println("Parentesi n." + depth);
            } else {
                //String[] returnValue = operation(total[depth], valueInput).split(" ");
                //total[depth] = Integer.parseInt(returnValue[1]);
                //total[depth] = operation(valueInput);
            }
        }
    }

    static String getValueFromUser() {
        String charset = "+-*/=().";
        Scanner input = new Scanner(System.in);
        String valueInput = input.nextLine();
        if (charset.contains(valueInput)) {
            operation(valueInput);
        } else {
            try {
                return ""+Integer.parseInt(valueInput);
            } catch (Exception e) {
                getValueFromUser();
            }
        }
        return "";
    }

    static int operation(String valueInput) {
        int total = 0;
        int val = 0;
        switch (valueInput) {
            case "+":
                val = Integer.parseInt(getValueFromUser());
                total += val;
                break;
            case "-":
                val = Integer.parseInt(valueInput);
                total -= val;
                break;
            case "*":
                val = Integer.parseInt(valueInput);
                total *= val;
                break;
            case "/":
                val = Integer.parseInt(valueInput);
                total /= val;
                break;
            case "%":
                val = Integer.parseInt(valueInput);
                total %= val;
                break;
            case "canc":
                total = 0;
                break;
            case "^":
                total = (int) Math.pow(total, Integer.parseInt(valueInput));
                break;
            case "(":
                getValueFromUser();
                break;
            case ")":
                return total;
            case "=":
                System.out.println("Questo è il totale: " + total +
                        "\nVuoi continuare? Scrivi ora . per uscire, altrimenti puoi continuare a digitare");
                break;
            default:
                total = Integer.parseInt(valueInput);

        }
        return total;
    }
}