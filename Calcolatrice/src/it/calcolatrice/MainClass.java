package it.calcolatrice;

import java.util.Scanner;

public class MainClass {
    public static void main(String[] args) {
        boolean exitCondition = false;
        int bracketTotal = 0;
        boolean bracket = false;
        String valueInput;
        Scanner input = new Scanner(System.in);

        System.out.println("Benvenuto nella calcolatrice." +
                "\nScrivi . per uscire altrimenti inizia a digitare!\nAttenzione, potrai inserire fino a 3 parentesi!!");
        int[] total = {0, 0, 0, 0, 0};
        String op = "";
        int depth = 0;
        int subStringIndex;
        String[] expressionStrings = {"", "", "", "", ""};

        while (!(valueInput = input.next()).equals(".")) {
            subStringIndex = 0;
            depth = searchForParentheses(valueInput);
            expressionStrings[subStringIndex] = valueInput;
            do {
                if(depth!=0){
                    expressionStrings[subStringIndex+1] = getSubString(expressionStrings[subStringIndex], depth--);
                    subStringIndex++;
                }
            }while(depth != 0);
            do{
                total[subStringIndex] = evaluateExpression(expressionStrings[subStringIndex]);
                System.out.println("totale: " + total[subStringIndex]);

                for(int i = subStringIndex; i>=0; i--){
                    for(int j = 0; j<=i; j++){
                        if(total[i]!=0){
                            expressionStrings[j] = expressionStrings[j].replace("("+expressionStrings[i]+")", ""+total[i]);
                        }
                    }
                }
                subStringIndex--;
            }while(subStringIndex >= 0);

        }
    }
    String[] updateExpressions(String[] expressions, int index, int[] total){
        for(int i = 4; i > 0; i-- ){
            for(int j = 0; j < i; j++){
                expressions[j] = expressions[j].replace("("+expressions[i+1]+")", ""+total[i+1]);
            }
        }
        return expressions;
    }
    static int evaluateExpression(String input){
        int total = 0;
        int val = 0;
        int indexNextValue = 0;
        boolean isFirstValue = true;
        for(int i = 0; i < input.length() && isFirstValue; i++){

            switch (input.charAt(i)) {
                case '+':
                    if(isFirstValue){
                        total = Integer.parseInt(input.substring(0,i));
                        isFirstValue = false;
                    }

                    indexNextValue = i+1;
                    total+=evaluateExpression(input.substring(indexNextValue));
                    break;
                case '-':
                    if(isFirstValue){
                        total = Integer.parseInt(input.substring(0,i));
                        isFirstValue = false;
                    }

                    indexNextValue = i+1;
                    total-=evaluateExpression(input.substring(indexNextValue));
                    break;
                case '*':
                    if(isFirstValue){
                        total = Integer.parseInt(input.substring(0,i));
                        isFirstValue = false;
                    }

                    indexNextValue = i+1;
                    total*=evaluateExpression(input.substring(indexNextValue));
                    break;
                case '/':
                    if(isFirstValue){
                        total = Integer.parseInt(input.substring(0,i));
                        isFirstValue = false;
                    }

                    indexNextValue = i+1;
                    total/=evaluateExpression(input.substring(indexNextValue));
                    break;
                case '%':
                    if(isFirstValue){
                        total = Integer.parseInt(input.substring(0,i));
                        isFirstValue = false;
                    }

                    indexNextValue = i+1;
                    total%=evaluateExpression(input.substring(indexNextValue));
                    break;
            }
        }
        try{
            if(total == 0){
                total = Integer.parseInt(input);
            }
            return total;
        }catch(Exception e){
            System.out.println("Gli errori possono accadere...");
            return 0;
        }
    }

    static int searchForParentheses(String input){
        if(!input.contains("(")){
            return 0;
        }else{
            int depth = 0;
            for(int i = 0; i<input.length(); i++){
                if(input.charAt(i) == '('){
                    depth++;
                }
            }
            return depth;
        }
    }
    static String getSubString(String input, int depth){
        if(input.isEmpty()){
            return "";
        }

        int startIndex = 0;
        int endIndex;

        while(input.charAt(startIndex) != '(' && depth != 0){
            startIndex++;
        }
        endIndex = startIndex;
        while(depth != 0){
            endIndex++;
            if(input.charAt(endIndex) == ')'){
                depth--;
            }
        }
        return input.substring(startIndex+1, endIndex);
    }
}