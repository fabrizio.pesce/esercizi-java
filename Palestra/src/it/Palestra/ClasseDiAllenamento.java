package it.Palestra;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ClasseDiAllenamento {
    private String nomeClasse;
    private LocalTime orario;
    List<Membro> listaMembri;
    private Istruttore istruttoreAssegnato;
    private int maxMembri;
    private DayOfWeek giornoDellaSettimana;
    private HashMap<Membro, Integer> valutazioniClasse;

    /**
     * Costruttore generico, non richiede l'istruttore perché verrà assegnato successivamente.
     * @param nomeClasse String
     * @param orario LocalTime
     * @param maxMembri int
     * @param giornoDellaSettimana DayOfWeek
     */
    public ClasseDiAllenamento(String nomeClasse, LocalTime orario, int maxMembri, DayOfWeek giornoDellaSettimana) {
        this.nomeClasse = nomeClasse;
        this.orario = orario;
        this.istruttoreAssegnato = null;
        this.maxMembri = maxMembri;
        listaMembri = new ArrayList<>();
        this.giornoDellaSettimana = giornoDellaSettimana;
        valutazioniClasse = new HashMap<>();
    }

    /**
     * Metodo che restituisce il nome della classe
     * @return String
     */
    public String getNomeClasse() {
        return nomeClasse;
    }
    /**
     * Metodo che restituisce l'orario della lezione
     * @return String
     */
    public LocalTime getOrario() {
        return orario;
    }
    /**
     * Metodo che restituisce la lista dei membri
     * @return List Membro
     */
    public List<Membro> getListaMembri() {
        return listaMembri;
    }
    /**
     * Metodo che restituisce l'istruttore assegnato alla classe
     * @return Istruttore
     */
    public Istruttore getIstruttoreAssegnato() {
        return istruttoreAssegnato;
    }
    /**
     * Metodo che restituisce il numero massimo di membri
     * @return int
     */
    public int getMaxMembri() {
        return maxMembri;
    }
    /**
     * Metodo che restituisce il giorno della settimana della lezione
     * @return DayOfWeek
     */
    public DayOfWeek getGiornoDellaSettimana() {
        return giornoDellaSettimana;
    }
    /**
     * Metodo che restituisce l'Hashmap relativa alle valutazioni della classe
     * @return HashMap Membro, Integer
     */
    public HashMap<Membro, Integer> getValutazioniClasse() {
        return valutazioniClasse;
    }

    /**
     * Metodo che imposta il nome della classe
     * @param nomeClasse String
     */
    public void setNomeClasse(String nomeClasse) {
        this.nomeClasse = nomeClasse;
    }

    /**
     * Metodo che imposta l'orario della classe
     * @param orario LocalTime
     */
    public void setOrario(LocalTime orario) {
        this.orario = orario;
    }

    /**
     * Metodo che imposta la lista dei membri
     * @param listaMembri List Membro
     */
    public void setListaMembri(List<Membro> listaMembri) {
        this.listaMembri = listaMembri;
    }

    /**
     * Metodo che imposta l'istruttore assegnato alla classe
     * @param istruttoreAssegnato Istruttore
     */
    public void setIstruttoreAssegnato(Istruttore istruttoreAssegnato) {
        this.istruttoreAssegnato = istruttoreAssegnato;
    }

    /**
     * Metodo che imposta il numero massimo di membri della classe
     * @param maxMembri int
     */
    public void setMaxMembri(int maxMembri) {
        this.maxMembri = maxMembri;
    }

    /**
     * Metodo che imposta il giorno della settimana della classe
     * @param giornoDellaSettimana DayOfWeek
     */
    public void setGiornoDellaSettimana(DayOfWeek giornoDellaSettimana) {
        this.giornoDellaSettimana = giornoDellaSettimana;
    }

    /**
     * Metodo che imposta la HashMap delle valutazioni
     * @param valutazioniClasse HashMap Membro, Integer
     */
    public void setValutazioniClasse(HashMap<Membro, Integer> valutazioniClasse) {
        this.valutazioniClasse = valutazioniClasse;
    }

    /**
     * Metodo che aggiunge un membro ad una classe
     * @param membro Membro
     * @throws ClassePienaException
     */
    public void addMember(Membro membro) throws ClassePienaException{
        if(this.listaMembri.size() < maxMembri) {
            listaMembri.add(membro);
        }else{
            throw new ClassePienaException("Impossibile aggiungere un nuovo membro.\nClasse piena");
        }
    }

    /**
     * Metodo che restituisce vero se il membro è iscritto alla classe, in caso contrario falso
     * @param membroDaCercare Membro
     * @return boolean
     */
    public boolean isMemberSubscribed(Membro membroDaCercare){
        for(Membro membro : listaMembri){
            //Scorro tutti i membri della lista finché non trovo il membro che mi serve
            if(membroDaCercare == membro){
                return true;
            }
        }
        return false;
    }

    /**
     * Metodo che aggiunge la valutazione all'hashmap e la associa al membro
     * @param member Member
     * @param rate int
     */
    public void addRate(Membro member, int rate){
        try{
            //Controllo se il membro è iscritto
            if(!isMemberSubscribed(member)){
                throw new MembroIscrittoException("Il membro non è iscritto a questa classe");
            }
            this.valutazioniClasse.put(member, rate);
        }catch (MembroIscrittoException membroIscrittoException){
            System.out.println(membroIscrittoException.getMessage());
        }
    }
}
