package it.Palestra;

/**
 * Classe che gestisce se la classe di allenamento è piena
 */
public class ClassePienaException extends Exception{
    public ClassePienaException(String message) {
        super(message);
    }

    public ClassePienaException() {
    }
}
