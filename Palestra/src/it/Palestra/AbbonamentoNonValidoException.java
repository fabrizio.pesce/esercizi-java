package it.Palestra;

/**
 * Classe che gestisce se l'abbonamento non è valido
 */
public class AbbonamentoNonValidoException extends Exception{
    public AbbonamentoNonValidoException() {
    }

    public AbbonamentoNonValidoException(String message) {
        super(message);
    }
}
