package it.Palestra;

/**
 * Classe che gestisce se il membro è iscritto
 */
public class MembroIscrittoException extends Exception{
    public MembroIscrittoException() {
    }

    public MembroIscrittoException(String message) {
        super(message);
    }
}
