package it.Palestra;

import java.time.LocalDate;

public class Membro {
    private String nome;
    private String cognome;
    private String idMembro;
    private TipoAbbonamento tipoAbbonamento;
    public enum TipoAbbonamento{
        giornaliero,
        mensile,
        annuale
    }
    private LocalDate dataEmissioneAbbonamento;

    /**
     * Costruttore che inizializza il membro con già un id e un tipo di abbonamento
     * @param nome String
     * @param cognome String
     * @param idMembro String
     * @param tipoAbbonamento TipoAbbonamento
     */
    public Membro(String nome, String cognome, String idMembro, TipoAbbonamento tipoAbbonamento) {
        this.nome = nome;
        this.cognome = cognome;
        this.idMembro = idMembro;
        this.tipoAbbonamento = tipoAbbonamento;
        this.dataEmissioneAbbonamento = null;
    }

    /**
     * Costruttore che inizializza il membro senza id e tipo abbonamento
     * @param nome String
     * @param cognome String
     */
    public Membro(String nome, String cognome) {
        this.nome = nome;
        this.cognome = cognome;
        this.idMembro = null;
        this.tipoAbbonamento = null;
        this.dataEmissioneAbbonamento = null;
    }

    /**
     * Costruttore che inizializza il membro con l'id ma senza tipo di abbonamento
     * @param nome String
     * @param cognome String
     * @param idMembro String
     */
    public Membro(String nome, String cognome, String idMembro) {
        this.nome = nome;
        this.cognome = cognome;
        this.idMembro = idMembro;
        this.tipoAbbonamento = null;
        this.dataEmissioneAbbonamento = null;
    }

    /**
     * Metodo che ritorna il nome del membro
     * @return String
     */
    public String getNome() {
        return nome;
    }
    /**
     * Metodo che ritorna il cognome del membro
     * @return String
     */
    public String getCognome() {
        return cognome;
    }
    /**
     * Metodo che ritorna l'id del membro
     * @return String
     */
    public String getIdMembro() {
        return idMembro;
    }
    /**
     * Metodo che ritorna il tipo di abbonamento
     * @return TipoAbbonamento
     */
    public TipoAbbonamento getTipoAbbonamento() {
        return tipoAbbonamento;
    }
    /**
     * Metodo che ritorna la data di emissione dell'abbonamento
     * @return LocalDate
     */
    public LocalDate getDataEmissioneAbbonamento() {
        return dataEmissioneAbbonamento;
    }

    /**
     * Metodo che imposta il nome del membro
     * @param nome String
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Metodo che imposta il cognome del membro
     * @param cognome String
     */
    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    /**
     * Metodo che imposta l'id del membro
     * @param idMembro String
     */
    public void setIdMembro(String idMembro) {
        this.idMembro = idMembro;
    }

    /**
     * Metodo che imposta il tipo di abbonamento del membro
     * @param tipoAbbonamento TipoAbbonamento
     */
    public void setTipoAbbonamento(TipoAbbonamento tipoAbbonamento) {
        this.tipoAbbonamento = tipoAbbonamento;
    }

    /**
     * Metodo che imposta la data di emissione dell'abbonamento del membro
     * @param dataEmissioneAbbonamento LocalDate
     */
    public void setDataEmissioneAbbonamento(LocalDate dataEmissioneAbbonamento) {
        this.dataEmissioneAbbonamento = dataEmissioneAbbonamento;
    }
}

