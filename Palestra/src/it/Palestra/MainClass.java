package it.Palestra;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;

public class MainClass {
    public static void main(String[] args) {
        //Istanzio due membri
        Membro membro1 = new Membro("Fabrizio", "Pesce");
        Membro membro2 = new Membro("Andrea", "Rossi");
        //Istanzio la mia gestione della palestra
        GestionePalestra gestionePalestra = new GestionePalestra();
        //Faccio iscrivere due nuovi membri
        gestionePalestra.subscribeNewMember(membro1, Membro.TipoAbbonamento.mensile);
        gestionePalestra.subscribeNewMember(membro2, Membro.TipoAbbonamento.mensile);
        //Modifico la data di emissione dell'abbonamento per fare una prova
        membro1.setDataEmissioneAbbonamento(LocalDate.parse("2024-01-20"));
        //Istanzio una classe di allenamento
        ClasseDiAllenamento classeDiAllenamento = new ClasseDiAllenamento("MMA", LocalTime.parse("22:00"), 3, DayOfWeek.MONDAY);
        //Istanzio un nuovo istruttore
        Istruttore istruttore = new Istruttore("Fabio", "Galeazzi", "0", "MMA", 15);

        //Provo le funzioni
        //Creo una nuova classe di allenamento nella gestione palestra
        gestionePalestra.createNewClass(classeDiAllenamento, istruttore);
        //Iscrivo i membri alla classe di allenamento(membro1 non verrà iscritto poiché gli è scaduta l'iscrizione)
        gestionePalestra.subscribeToClass(classeDiAllenamento, membro1);
        gestionePalestra.subscribeToClass(classeDiAllenamento, membro2);
        //Stampo le classi nel determinato giorno
        gestionePalestra.printClassInDate(DayOfWeek.MONDAY);
        //Inserisco delle valutazioni fatte dai membri(quella di membro 1 non verrà accettata poiché non è iscritto)
        gestionePalestra.insertRates(classeDiAllenamento, membro1, 4);
        gestionePalestra.insertRates(classeDiAllenamento, membro2, 10);
        //Calcola la media delle varie classi
        gestionePalestra.meanRates();
    }

}
