package it.Palestra;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class GestionePalestra {
    HashMap<String, Membro.TipoAbbonamento> membriAbbonati;
    List<ClasseDiAllenamento> listaClasseDiAllenamento;
    //Ho deciso che l'id del membro verrà incrementato ad ogni iscrizione.
    static int idMembro = 0;

    /**
     * Costruttore che inizializza le hashmap
     */
    public GestionePalestra(){
        membriAbbonati = new HashMap<>();
        listaClasseDiAllenamento = new LinkedList<>();
    }


    /**
     * Metodo che ritorna l'elenco dei membri abbonati
     * @return HashMap String, Membro.TipoAbbonamento
     */
    public HashMap<String, Membro.TipoAbbonamento> getMembriAbbonati() {
        return membriAbbonati;
    }

    /**
     * Metodo che ritorna la lista delle classi di allenamento
     * @return List ClasseDiAllenamento
     */
    public List<ClasseDiAllenamento> getClassiDiAllenamento() {
        return listaClasseDiAllenamento;
    }

    /**
     * Metodo che imposta l'elenco dei membri abbonati
     * @param membriAbbonati HashMap String, Membro.TipoAbbonamento
     */
    public void setMembriAbbonati(HashMap<String, Membro.TipoAbbonamento> membriAbbonati) {
        this.membriAbbonati = membriAbbonati;
    }

    /**
     * Metodo che imposta la lista delle classi di allenamento
     * @param classeDiAllenamento List ClasseDiAllenamento
     */
    public void setClassiDiAllenamento(List<ClasseDiAllenamento> classeDiAllenamento) {
        this.listaClasseDiAllenamento = classeDiAllenamento;
    }

    /**
     * Metodo che permette ad un nuovo mebro di iscriversi.
     * A questo verrà assegnato un id, il tipo di abbonamento e come data di emissione quella odierna
     * @param membro Membro
     * @param tipoAbbonamento Membro.TipoAbbonamento
     */
    public void subscribeNewMember(Membro membro, Membro.TipoAbbonamento tipoAbbonamento){
        membro.setIdMembro(""+idMembro++);
        membro.setTipoAbbonamento(tipoAbbonamento);
        membro.setDataEmissioneAbbonamento(LocalDate.now());
        membriAbbonati.put(membro.getIdMembro(), tipoAbbonamento);
    }

    /**
     * Metodo che assegna un istruttore alla classe di allenamento per poi inserirla nella lista
     * @param classeDiAllenamento ClasseDiAllenamento
     * @param istruttore Istruttore
     */
    public void createNewClass(ClasseDiAllenamento classeDiAllenamento, Istruttore istruttore){
        classeDiAllenamento.setIstruttoreAssegnato(istruttore);
        this.listaClasseDiAllenamento.add(classeDiAllenamento);
    }

    /**
     * Metodo che permette ad un membro di iscriversi ad una classe di allenamento.
     * @param classeDiAllenamento ClasseDiAllenamento
     * @param membro Membro
     */
    public void subscribeToClass(ClasseDiAllenamento classeDiAllenamento, Membro membro){
        for(ClasseDiAllenamento classe : this.listaClasseDiAllenamento){
            try{
                //Controllo se il membro è già iscritto alla classe, in caso positivo lancio l'exception relativa
                if(classe.isMemberSubscribed(membro)){
                    throw new MembroIscrittoException("Il membro è già iscritto a questa classe");
                }
                //Controllo se l'iscrizione è valida
                isSubscriptionValid(membro);
                //Controllo se la classe richiesta è nell'elenco delle classi
                if(classe == classeDiAllenamento){
                    classe.addMember(membro);
                }

            }catch(ClassePienaException classePienaException){
                System.out.println(classePienaException.getMessage());
            }catch(MembroIscrittoException membroIscrittoException){
                System.out.println(membroIscrittoException.getMessage());
            }catch(AbbonamentoNonValidoException abbonamentoNonValidoException){
                System.out.println(abbonamentoNonValidoException.getMessage());
            }
        }
    }

    /**
     * Metodo che controlla se l'abbonamento di un membro è ancora valido
     * @param membro Membro
     * @throws AbbonamentoNonValidoException
     */
    public void isSubscriptionValid(Membro membro) throws AbbonamentoNonValidoException {
        switch (membro.getTipoAbbonamento()){
            case giornaliero:
                //Controllo se la data di emissione+un giorno è una data è già passata rispetto a quella odierna
                if(membro.getDataEmissioneAbbonamento().plusDays(1).isBefore(LocalDate.now())){
                    throw new AbbonamentoNonValidoException("Abbonamento giornaliero scaduto");
                }else{
                    System.out.println("OK");
                }
                break;
            case mensile:
                //Controllo se la data di emissione+un mese è una data è già passata rispetto a quella odierna
                if(membro.getDataEmissioneAbbonamento().plusMonths(1).isBefore(LocalDate.now())){
                    throw new AbbonamentoNonValidoException("Abbonamento mensile scaduto");
                }else{
                    System.out.println("OK");
                }
                break;
            case annuale:
                //Controllo se la data di emissione+un anno è una data è già passata rispetto a quella odierna
                if(membro.getDataEmissioneAbbonamento().plusYears(1).isBefore(LocalDate.now())){
                    throw new AbbonamentoNonValidoException("Abbonamento annuale scaduto");
                }else{
                    System.out.println("OK");
                }
                break;
            default:
                //Altrimenti l'abbonamento non è riconosciuto
                throw new AbbonamentoNonValidoException("Abbonamento non riconosciuto");

        }
    }

    /**
     * Metodo che stampa le classi dato un determinato giorno della settimana.
     * @param inputDay DayOfWeek
     */
    public void printClassInDate(DayOfWeek inputDay){
        for(ClasseDiAllenamento classeDiAllenamento : listaClasseDiAllenamento){
            if(classeDiAllenamento.getGiornoDellaSettimana().equals(inputDay)){
                System.out.println("Lezione: "+classeDiAllenamento.getNomeClasse()
                        +"\nNome istruttore: "+classeDiAllenamento.getIstruttoreAssegnato().getNome()
                        +"\nPosti disponibili: "+(classeDiAllenamento.getMaxMembri()-classeDiAllenamento.getListaMembri().size()));
            }
        }
    }

    /**
     * Metodo che inserisce all'interno della classe la valutazione fatta da un membro
     * @param classeScelta ClasseDiAllenamento
     * @param membro Membro
     * @param rate int
     */
    public void insertRates(ClasseDiAllenamento classeScelta, Membro membro, int rate){
        for(ClasseDiAllenamento classeDiAllenamento : listaClasseDiAllenamento){
            //Scorro tutte le classi della lista finché non trovo quella scelta
            if(classeDiAllenamento == classeScelta){
                classeDiAllenamento.addRate(membro, rate);
                //Faccio la return per evitare cicli non necessari
                return;
            }
        }
    }

    /**
     * Metodo che calcola e stampa la media di ogni classe di allenamento
     */
    public void meanRates(){
        //Ciclo ogni classe di allenamento
        for(ClasseDiAllenamento classeDiAllenamento : listaClasseDiAllenamento){
            int total = 0;
            //Ciclo ogni valutazione della classe di allenamento
            for(int value : classeDiAllenamento.getValutazioniClasse().values()){
                total += value;
            }
            System.out.println("La media di valutazioni di " +classeDiAllenamento.getNomeClasse()+
                    " è: "+(total/classeDiAllenamento.getValutazioniClasse().size()));
        }
    }
}
