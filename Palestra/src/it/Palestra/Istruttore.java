package it.Palestra;

public class Istruttore extends Membro{
    private String specializzazione;
    private int anniDiEsperienza;

    /**
     * Costruttore che inizializza l'istruttore, avrà automaticamente l'abbonamento null e la data di iscrizione a null.
     * @param nome String
     * @param cognome String
     * @param idMembro String
     * @param specializzazione String
     * @param anniDiEsperienza String
     */
    public Istruttore(String nome, String cognome, String idMembro, String specializzazione, int anniDiEsperienza) {
        super(nome, cognome, idMembro);
        this.specializzazione = specializzazione;
        this.anniDiEsperienza = anniDiEsperienza;
    }

    /**
     * Metodo che ritorna la specializzazione dell'istruttore
     * @return String
     */
    public String getSpecializzazione() {
        return specializzazione;
    }
    /**
     * Metodo che ritorna gli anni di esperienza dell'istruttore
     * @return String
     */
    public int getAnniDiEsperienza() {
        return anniDiEsperienza;
    }
    /**
     * Metodo che imposta la specializzazione dell'istruttore
     */
    public void setSpecializzazione(String specializzazione) {
        this.specializzazione = specializzazione;
    }
    /**
     * Metodo che imposta gli anni di esperienza dell'istruttore
     */
    public void setAnniDiEsperienza(int anniDiEsperienza) {
        this.anniDiEsperienza = anniDiEsperienza;
    }
}
