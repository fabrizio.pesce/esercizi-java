package it.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PariDispariTest {
    @Test
    void testPari(){
        assertTrue(PariDispari.pariDispari(4,4));
        assertFalse(PariDispari.pariDispari(5,5));
        assertTrue(PariDispari.pariDispari(5,4));
        assertFalse(PariDispari.pariDispari(4,5));
        assertTrue(PariDispari.pariDispari(0,4));
        assertFalse(PariDispari.pariDispari(-2,5));
    }
}
