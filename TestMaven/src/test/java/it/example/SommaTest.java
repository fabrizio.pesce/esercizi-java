package it.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SommaTest {

    @Test
    void testSommaDueNumeri(){
        assertEquals(10,Somma.somma_numeri(7,3));
        assertEquals(10, Somma.somma_numeri(5,4));
    }

}