package it.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PasswordVerifierTest {

    @Test
    void passwordVerifier(){
        assertTrue(PasswordVerifier.passwordVerifier("Ciao0000"));
        assertFalse(PasswordVerifier.passwordVerifier("Ciao0"));
        assertFalse(PasswordVerifier.passwordVerifier("Ci0"));
        assertFalse(PasswordVerifier.passwordVerifier(""));
        assertFalse(PasswordVerifier.passwordVerifier("ioioioio"));
        assertFalse(PasswordVerifier.passwordVerifier("IIIIIIIIII"));
        assertFalse(PasswordVerifier.passwordVerifier("ioioioio0"));
        assertTrue(PasswordVerifier.passwordVerifier("Ioioioio0"));
        assertFalse(PasswordVerifier.passwordVerifier(null));
    }

    @Test
    void passwordValida(){
        assertTrue(PasswordVerifier.valida("Ciao0000"));
        assertFalse(PasswordVerifier.valida("Ciao0"));
        assertFalse(PasswordVerifier.valida("Ci0"));
        assertFalse(PasswordVerifier.valida(""));
        assertFalse(PasswordVerifier.valida("ioioioio"));
        assertFalse(PasswordVerifier.valida("IIIIIIIIII"));
        assertFalse(PasswordVerifier.valida("ioioioio0"));
        assertTrue(PasswordVerifier.valida("Ioioioio0"));

    }

}