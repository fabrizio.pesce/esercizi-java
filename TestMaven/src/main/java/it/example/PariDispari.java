package it.example;

public class PariDispari {
    public static boolean pariDispari(int a, int b){
        if(((a%2)==0) && ((b%2)==0)){
            return true;
        }
        if(((a%2)!=0) && ((b%2)!=0)){
            return false;
        }
        return a > b;
    }
}
