package it.example;

public class PasswordVerifier {
    public static boolean passwordVerifier(String password){
        //return password.matcher.("[A-Z]") && password.matches("[a-z]") && password.matches("[0-9]");
        if(password == null || password.length()<8){
            return false;
        }

        int upperCaseFlag = 0;
        int lowerCaseFlag = 0;
        int numberFlag = 0;

        for (int i = 0; i < password.length(); i++) {
            if((password.charAt(i)+"").matches("[A-Z]")){
                upperCaseFlag++;
            }else if((password.charAt(i)+"").matches("[a-z]")){
                lowerCaseFlag++;
            }else if((password.charAt(i)+"").matches("[0-9]")){
                numberFlag++;
            }
        }
        return (upperCaseFlag>0) && (lowerCaseFlag>0) && (numberFlag>0);
    }

    public static boolean valida(String passwordDaValidare){
        boolean trovatoNumero = false;
        boolean trovatoMaiuscola = false;
        boolean trovatoMinuscola = false;

        for (int i = 0; i < passwordDaValidare.length(); i++) {
            Character carattereDaVedere = passwordDaValidare.charAt(i);

            if (Character.isDigit(carattereDaVedere)) {
                trovatoNumero = true;
            }
            if (Character.isUpperCase(carattereDaVedere)) {
                trovatoMaiuscola = true;
            }
            if (Character.isLowerCase(carattereDaVedere)) {
                trovatoMinuscola = true;
            }
        }
        if(!trovatoNumero || !trovatoMaiuscola || !trovatoMinuscola || passwordDaValidare.length() < 8){
            return false;
        }
        return true;
    }
}
