package it.FabrizioPesce;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VeicoloBaseTest {

    VeicoloBase veicolo1;
    Accessorio accessorio1;
    Accessorio accessorio2;
    Accessorio accessorio3;

    @BeforeEach
    void setUp(){
        veicolo1 = new VeicoloBase("Macchina", 10, 20000.00, 2);
        accessorio1 = new Accessorio("Navigatore", 200.00);
        accessorio2 = new Accessorio("Sensore del sonno", 3000.00);
        accessorio3 = new Accessorio("Guida assistita", 6500.00);
    }

    /**
     * Test per controllare che la mia funzione ritorni un valore corretto di velocità
     */
    @Test
    void testCalcolaVelocitaMassima(){
        //Mi aspetto che il valore che ritorni la velocità massima sia quello della mia formula
        assertEquals((10*30)/3.5, veicolo1.calcolaVelocitaMassima());
        assertNotEquals(30, veicolo1.calcolaVelocitaMassima());
        assertNotEquals(0, veicolo1.calcolaVelocitaMassima());
        assertNotEquals(null, veicolo1.calcolaVelocitaMassima());
    }

    /**
     * Test per controllare calcolare il costo totale del veicolo man mano che gli aggiungo gli accessori
     */
    @Test
    void testCalcolaCostoTotale(){
        assertEquals(20000, veicolo1.calcolaCostoTotale());
        veicolo1.aggiungiAccessorio(accessorio1);
        assertEquals(20200, veicolo1.calcolaCostoTotale());
        veicolo1.aggiungiAccessorio(accessorio2);
        assertEquals(23200, veicolo1.calcolaCostoTotale());
        veicolo1.aggiungiAccessorio(accessorio3);
        assertNotEquals(29700, veicolo1.calcolaCostoTotale());
    }

}