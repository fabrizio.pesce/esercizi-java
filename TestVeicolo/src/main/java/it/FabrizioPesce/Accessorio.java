package it.FabrizioPesce;

/**
 * Classe che definisce gli accessori
 */
public class Accessorio {
    private String nome;
    private double prezzo;

    /**
     * Costruttore della classe Accessorio
     * @param nome String
     * @param prezzo double
     */
    public Accessorio(String nome, double prezzo) {
        this.nome = nome;
        this.prezzo = prezzo;
    }

    /**
     * Metodo che ritorna il nome dell'accessorio
     * @return String
     */
    public String getNome() {
        return nome;
    }

    /**
     * Metodo che ritorna il prezzo dell'accessorio
     * @return double
     */
    public double getPrezzo() {
        return prezzo;
    }

    /**
     * Metodo che imposta il nome dell'accessorio
     * @param nome String
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Metodo che imposta il prezzo dell'accessorio
     * @param prezzo double
     */
    public void setPrezzo(double prezzo) {
        this.prezzo = prezzo;
    }
}
