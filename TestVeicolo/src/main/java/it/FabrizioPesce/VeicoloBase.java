package it.FabrizioPesce;

/**
 * Classe per un Veicolo base
 */
public class VeicoloBase {
    private String tipoVeicolo;
    private int potenzaMotore;
    private Accessorio[] accessori;
    private int numAccessori;
    private double prezzo;
    private int maxAccessori;

    /**
     * Costruttore della classe veicolo base, è stato aggiunto maxAccessori per permettere la gestione degli accessori.
     * @param tipoVeicolo String
     * @param potenzaMotore int
     * @param prezzo double
     * @param maxAccessori int
     */
    public VeicoloBase(String tipoVeicolo, int potenzaMotore, double prezzo, int maxAccessori) {
        this.tipoVeicolo = tipoVeicolo;
        this.potenzaMotore = potenzaMotore;
        this.accessori = new Accessorio[maxAccessori];
        this.numAccessori = 0;
        this.prezzo = prezzo;
        this.maxAccessori = maxAccessori;
    }

    /**
     * Metodo che restituisce il tipo di veicolo
     * @return String
     */
    public String getTipoVeicolo() {
        return tipoVeicolo;
    }

    /**
     * Metodo che restituisce la potenza del motore
     * @return int
     */
    public int getPotenzaMotore() {
        return potenzaMotore;
    }

    /**
     * Metodo che restituisce l'array che contiene gli accessori
     * @return Accessorio[]
     */
    public Accessorio[] getAccessori() {
        return accessori;
    }
    /**
     * Metodo che restituisce il numero di accessori
     * @return int
     */
    public int getNumAccessori() {
        return numAccessori;
    }
    /**
     * Metodo che restituisce il prezzo
     * @return double
     */
    public double getPrezzo() {
        return prezzo;
    }
    /**
     * Metodo che restiuscie il numero massimo di accessori
     * @return int
     */
    public int getMaxAccessori() {
        return maxAccessori;
    }

    /**
     * Metodo che imposta il tipo del veicolo
     * @param tipoVeicolo String
     */
    public void setTipoVeicolo(String tipoVeicolo) {
        this.tipoVeicolo = tipoVeicolo;
    }

    /**
     * Metodo che imposta la potenza del motore
     * @param potenzaMotore int
     */
    public void setPotenzaMotore(int potenzaMotore) {
        this.potenzaMotore = potenzaMotore;
    }

    /**
     * Metodo che imposta l'array di accessori
     * @param accessori Accessorio[]
     */
    public void setAccessori(Accessorio[] accessori) {
        this.accessori = accessori;
    }

    /**
     * Metodo che imposta il numero di accessori
     * @param numAccessori int
     */
    public void setNumAccessori(int numAccessori) {
        this.numAccessori = numAccessori;
    }


    /**
     * Metodo che imposta il prezzo di un accessorio
     * @param prezzo double
     */
    public void setPrezzo(double prezzo) {
        this.prezzo = prezzo;
    }

    /**
     * Metodo che imposta il numero massimo di accessori
     * @param maxAccessori int
     */
    public void setMaxAccessori(int maxAccessori) {
        this.maxAccessori = maxAccessori;
    }

    /**
     * Metodo che aggiunge un accessorio al veicolo
     * @param accessorio Accessorio
     */
    public void aggiungiAccessorio(Accessorio accessorio){
        //Controllo se ho raggiunto il numero massimo di accessori
        if(numAccessori < maxAccessori){
            accessori[numAccessori] = accessorio;
            numAccessori++;
            System.out.println("Accessorio aggiunto");
        }else{
            System.out.println("Impossibile aggiungere accessorio." +
                        "\nNumero massimo di accessori raggiunto");
        }
    }

    /**
     * Metodo che ritorna la velocità massima del veicolo
     * @return double
     */
    public double calcolaVelocitaMassima(){
        //non so quanto può essere veritiera la formula
        return (double) (potenzaMotore * 30)/3.5;
    }

    /**
     * Metodo che stampa gli accessori
     */
    public void stampaAccessori(){
        for(int i = 0; i<numAccessori; i++){
            System.out.println("Nome accessorio: "+accessori[i].getNome()+
                    " Prezzo: "+accessori[i].getPrezzo());
        }
    }

    /**
     * Metodo che ritorna il costo totale del veicolo considerati anche gli accessori
     * @return double
     */
    public double calcolaCostoTotale(){
        double totale = 0;
        for(int i = 0; i< numAccessori; i++){
            totale += accessori[i].getPrezzo();
        }
        totale += this.prezzo;
        return totale;
    }

    /**
     * Metodo che stampa una simulazione di un viaggio
     * @param distanza double
     * @param consumoMedio double
     */
    public void simulaViaggio(double distanza, double consumoMedio){
        System.out.println("Simulazione viaggio di "+distanza+":");
        System.out.println("Tempo: " + (this.calcolaVelocitaMassima()/distanza));
        System.out.println("Consumo: "+ (consumoMedio*distanza));
    }

    /**
     * metodo che mi stampa attraverso un numero random la velocità media
     */
    public void calcolaVelocitaMedia(){
        System.out.println(Math.random()*99+1);
    }

}
