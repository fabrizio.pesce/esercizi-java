package it.Libreria;

import java.util.ArrayList;
import java.util.List;

public class Biblioteca {
    List<Libro> elencoLibri;

    public Biblioteca() {
        this.elencoLibri = new ArrayList<>();
    }

    public List<Libro> getElencoLibri() {
        return this.elencoLibri;
    }

    public void addNewBook(Libro l){
        this.elencoLibri.add(l);
    }

    public Libro searchBook(String titolo){
        for(Libro libro : this.elencoLibri){
            if(libro.getTitolo().equals(titolo)){
                System.out.println(titolo);
                return libro;
            }
        }
        return null;
    }

    public void printBooks(){
        for(Libro libro : this.elencoLibri){
            libro.printBookInfo();
        }
        System.out.println("Il numero totale di pagine dei libri stampati è: "+numberOfAllPages());
    }

    public int numberOfAllPages(){
        int total = 0;
        for (Libro libro : elencoLibri) {
            if (libro instanceof LibroStampato) {
                total += ((LibroStampato) libro).getNumPagine();
            }
        }

        return total;
    }
}
