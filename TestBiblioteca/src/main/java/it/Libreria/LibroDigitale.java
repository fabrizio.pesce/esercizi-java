package it.Libreria;

public class LibroDigitale extends Libro{
    String formatoDigitale;

    public LibroDigitale(String titolo, String autore, int annoPubblicazione, String formatoDigitale) {
        super(titolo, autore, annoPubblicazione);
        this.formatoDigitale = formatoDigitale;
    }

    public String getFormatoDigitale() {
        return formatoDigitale;
    }

    public void setFormatoDigitale(String formatoDigitale) {
        this.formatoDigitale = formatoDigitale;
    }
}

