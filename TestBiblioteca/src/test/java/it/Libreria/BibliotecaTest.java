package it.Libreria;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BibliotecaTest {
    Biblioteca biblioteca;
    Libro libro;
    @BeforeEach
    void setUp(){
        libro = new Libro("Ciao", "Fabrizio", 2020);
        biblioteca = new Biblioteca();
    }

    @Test
    void verificaAggiunta(){
        biblioteca.addNewBook(libro);
        assertEquals(1, biblioteca.getElencoLibri().size());
        assertNotNull(biblioteca.searchBook("Ciao"));
    }
    @Test
    void verificaRicerca(){
        biblioteca.addNewBook(libro);
        assertNull(biblioteca.searchBook("Null"));
    }
}