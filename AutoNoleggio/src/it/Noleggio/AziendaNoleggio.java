package it.Noleggio;

import java.util.HashSet;

public class AziendaNoleggio {
    private HashSet<Auto> elencoAuto;

    public AziendaNoleggio() {
        this.elencoAuto = new HashSet<>();
    }

    public HashSet<Auto> getElencoAuto() {
        return elencoAuto;
    }

    public void aggiungiAuto(Auto auto) {
        this.elencoAuto.add(auto);
    }

    public void rimuoviAuto(String numeroTarga){
        Auto auto = cercaAuto(numeroTarga);
        if(auto != null) this.elencoAuto.remove(auto);
    }

    public Auto cercaAuto(String numeroTarga){
        for(Auto auto : this.elencoAuto){
            if(auto.getNumeroTarga().equals(numeroTarga)){
                return auto;
            }
        }
        System.out.println("Macchina con targa "+numeroTarga+" non presente\n");
        return null;
    }

    public void visualizzaAutoDisponibili(){
        for(Auto auto : this.elencoAuto){
            System.out.println("Marca: "+auto.getMarca()
                    +"\nModello: "+auto.getModello()
                    +"\nTarga: "+auto.getNumeroTarga()
                    +"\nPrezzo giornaliero: "+auto.getPrezzoGiornaliero()
                    +"\n");
        }
    }

    public double calcolaPrezzoNoleggio(String numeroTarga, int giorni){
        Auto auto = cercaAuto(numeroTarga);
        if(auto != null) {return auto.getPrezzoGiornaliero()*giorni;}
        return 0;
    }
}
