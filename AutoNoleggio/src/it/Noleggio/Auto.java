package it.Noleggio;

public class Auto {
    private String numeroTarga;
    private String marca;
    private String modello;
    private double prezzoGiornaliero;

    public Auto(String numeroTarga, String marca, String modello, double prezzoGiornaliero) {
        this.numeroTarga = numeroTarga;
        this.marca = marca;
        this.modello = modello;
        this.prezzoGiornaliero = prezzoGiornaliero;
    }

    public String getNumeroTarga() {
        return numeroTarga;
    }

    public String getMarca() {
        return marca;
    }

    public String getModello() {
        return modello;
    }

    public double getPrezzoGiornaliero() {
        return prezzoGiornaliero;
    }

    public void setNumeroTarga(String numeroTarga) {
        this.numeroTarga = numeroTarga;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public void setModello(String modello) {
        this.modello = modello;
    }

    public void setPrezzoGiornaliero(double prezzoGiornaliero) {
        this.prezzoGiornaliero = prezzoGiornaliero;
    }
}
