package it.Noleggio;

public class MainClass {
    public static void main(String[] args) {
        Auto auto1 = new Auto("AA123BB", "Alfa-Romeo", "Giulietta", 35.5);
        Auto auto2 = new Auto("AA124BB", "Peugeot", "208", 30.5);

        AziendaNoleggio aziendaNoleggio = new AziendaNoleggio();
        aziendaNoleggio.aggiungiAuto(auto1);
        aziendaNoleggio.aggiungiAuto(auto2);
        System.out.println("Il prezzo del noleggio è: "+aziendaNoleggio.calcolaPrezzoNoleggio("AA123BB", 5)+"\n");
        aziendaNoleggio.visualizzaAutoDisponibili();
        aziendaNoleggio.rimuoviAuto("AA12345");
        aziendaNoleggio.rimuoviAuto("AA123BB");
        aziendaNoleggio.rimuoviAuto("AA123BB");
        aziendaNoleggio.visualizzaAutoDisponibili();
    }
}
