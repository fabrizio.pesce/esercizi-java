package it.ProvaException;

import java.util.Scanner;

public class MainClass {
    public static void main(String[] args) {
        int[] array = new int[10];
        divisioneCasuale(8);
        array = inserimentoOltreLimite(array, 8, 7);
        checkFormatOfImaginaryNumber("5i2");
        Scanner input = new Scanner(System.in);
        for(int i = 0; i<4; i++){
            System.out.println("Inserire prima stringa, seconda stringa e il numero di caratteri da confrontare" +
                    "\nIl tutto separato da spazi");
            try{
                boolean checkStrings = stringheSimili(input.next(), input.next(), input.nextInt());
                System.out.println("Le due stringhe sono simili");
            }catch (IllegalArgumentException illegalArgumentException){
                System.out.println("Il numero passato è inferiore a 0\n"
                        +illegalArgumentException.getMessage());
            }catch (StringheDiverse stringheDiverse){
                System.out.println("Le due stringhe hanno valori diversi nei primi n posti");
            }catch(StringheVuote stringheVuote){
                System.out.println("Una o entrambe le stringhe sono vuote");
            } catch (Exception e) {
                System.out.println("Errore generico lmao");
            }
        }
    }
    public static void divisioneCasuale(int number){
        try{
            System.out.println("Risultato divisione: "+ (number/(int)(Math.random()*3)));
        }catch (ArithmeticException exception){
            System.out.println("Divisione per 0 effettuata :(");
        }
    }
    public static int[] inserimentoOltreLimite(int[] array, int valore, int posizione){
        try{
            array[posizione] = valore;
            System.out.println("Elemento "+valore+" inserito nella posizione "+posizione);
        }catch (IndexOutOfBoundsException exception){
            System.out.println("Sei andato out of bounds :(");
        }
        /*
        *Metodo alternativo senza l'utilizzo delle eccezioni:
        if(array.size()<posizione){
            array[posizione] = valore;
            System.out.println("Elemento "+valore+" inserito nella posizione"+posizione);
        }else{
            System.out.println("Sei andato out of bounds :(");
        }
         */
        return array;
    }

    public static void checkFormatOfImaginaryNumber(String number){
        try{
            if(number.contains("i")) {
                String subStringNumber = number.substring(1);
                Double.parseDouble(subStringNumber);
                System.out.println(number+" è un numero immaginario :D");
            }else{
                Double.parseDouble(number);
                System.out.println("è un numero reale :D");
            }
        }catch(NumberFormatException formatException){
            System.out.println("Hai passato una stringa formattata male :(\n"+formatException.getMessage());
        }
    }
    public static boolean stringheSimili(String a, String b, int n) throws StringheDiverse, StringheVuote{
        if(n<0){ throw new IllegalArgumentException("Numero errato"); }
        if(a.isEmpty() || b.isEmpty()){ throw new StringheVuote();}
        String stringaPiuLunga = a.length()>b.length() ? a : b;
        String stringaPiuCorta = a.length()<b.length() ? a : b;
        for(int i = 0; i < stringaPiuCorta.length() && i<n; i++){
            if(stringaPiuLunga.toLowerCase().charAt(i) != stringaPiuCorta.toLowerCase().charAt(i)){
                throw new StringheDiverse();
            }
        }
        return true;
    }
}
