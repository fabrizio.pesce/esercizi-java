package it.Libreria;

public class MainClass {
    public static void main(String[] args) {
        Biblioteca biblioteca = new Biblioteca();
        LibroStampato libro1 = new LibroStampato("Test", "Fabrizio", 2024, 300);
        LibroStampato libro2 = new LibroStampato("Clean Code", "Robert C. Martin", 2008, 431);
        LibroDigitale libro3 = new LibroDigitale("Fuffi il SQL", "Fabrizio Pesce", 2022, "PDF");
        biblioteca.addNewBook(libro1);
        biblioteca.addNewBook(libro2);
        biblioteca.addNewBook(libro3);
        biblioteca.searchBook("Clean Code").printBookInfo();
        biblioteca.printBooks();
    }
}
