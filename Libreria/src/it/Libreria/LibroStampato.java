package it.Libreria;

public class LibroStampato extends Libro{
    private int numPagine;
    public LibroStampato(String titolo, String autore, int annoPubblicazione, int numPagine) {
        super(titolo, autore, annoPubblicazione);
        this.numPagine = numPagine;
    }

    public int getNumPagine() {
        return numPagine;
    }

    public void setNumPagine(int numPagine) {
        this.numPagine = numPagine;
    }
}
