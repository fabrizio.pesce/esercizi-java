package it.aereoporto;

public class VoloNonDiretto extends Volo{
    private int maxScali;
    private Volo[] elencoScali;
    private int numScali;

    public VoloNonDiretto(String sigla, Aereoporto aereoportoPartenza, Aereoporto aereoportoArrivo, String aereomobile, int maxSeat, int maxScali) {
        super(sigla, aereoportoPartenza, aereoportoArrivo, aereomobile, maxSeat);
        this.maxScali = maxScali;
        elencoScali = new Volo[maxScali];
        numScali = 0;
    }


    public int getMaxScali() {
        return maxScali;
    }

    public Volo[] getelencoScali() {
        return elencoScali;
    }
    public void aggiungiScalo(Volo scalo){
        if(numScali <= maxScali){
            elencoScali[numScali] = scalo;
            numScali++;
        }
    }

    @Override
    public void printVolo() {
        super.printVolo();
        System.out.print("via ");
        for(int i = 0; i<numScali; i++){
            System.out.print(elencoScali[i].aereoportoPartenza.getCitta()+" "
                    +elencoScali[i].aereoportoPartenza.getNome()+" - "
                    +elencoScali[i].aereoportoArrivo.getCitta()+" "
                    +elencoScali[i].aereoportoArrivo.getNome());
        }
    }
}
