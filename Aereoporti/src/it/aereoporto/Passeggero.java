package it.aereoporto;

public class Passeggero{
    private String nome, nazionalita, siglaVolo, posto, pastoRichiesto;
    public Passeggero(String nome, String nazionalita, String siglaVolo, String posto, String pastoRichiesto){
        this.nome = nome;
        this.nazionalita = nazionalita;
        this.siglaVolo = siglaVolo;
        this.posto = posto;
        this.pastoRichiesto = pastoRichiesto;
    }

    public Passeggero(String nome, String nazionalita, String siglaVolo, String posto){
        this.nome = nome;
        this.nazionalita = nazionalita;
        this.siglaVolo = siglaVolo;
        this.posto = posto;
    }

    public String getNome(){
        return this.nome;
    }

    public String getNazionalita(){
        return this.nazionalita;
    }

    public String getSiglaVolo(){
        return this.siglaVolo;
    }

    public String getPosto(){
        return this.posto;
    }

    public String getPastoRichiesto(){
        return this.pastoRichiesto;
    }

    public void changeSeat(String newSeat){
        this.posto = newSeat;
    }
}
