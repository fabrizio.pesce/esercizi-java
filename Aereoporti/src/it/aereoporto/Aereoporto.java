package it.aereoporto;

public class Aereoporto{
    private String nome, citta, sigla;
    public Aereoporto(String nome, String citta, String sigla){
        this.nome = nome;
        this.citta = citta;
        this.sigla = sigla;
    }
    public String getNome(){
        return this.nome;
    }

    public String getCitta(){
        return this.citta;
    }

    public String getSigla(){
        return this.sigla;
    }

}
