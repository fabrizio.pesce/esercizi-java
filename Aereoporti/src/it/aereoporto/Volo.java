package it.aereoporto;

public class Volo {
    String sigla;
    Aereoporto aereoportoPartenza, aereoportoArrivo;
    String aereomobile;
    Passeggero[] elencoPasseggeri;
    int maxSeat;
    int numeroPasseggeri;

    public Volo(String sigla, Aereoporto aereoportoPartenza, Aereoporto aereoportoArrivo, String aereomobile, int maxSeat) {
        numeroPasseggeri = 0;
        this.sigla = sigla;
        this.aereoportoPartenza = aereoportoPartenza;
        this.aereoportoArrivo = aereoportoArrivo;
        this.aereomobile = aereomobile;
        try {
            this.maxSeat = maxSeat;
        } catch (Exception e) {
            System.out.println("Errore nell'inserimento del numero massimo");
        }
        this.elencoPasseggeri = new Passeggero[maxSeat];
    }

    public void addPassenger(Passeggero nuovoPasseggero) {
        this.elencoPasseggeri[numeroPasseggeri] = nuovoPasseggero;
        this.numeroPasseggeri++;
    }

    public void printVolo() {
        System.out.println("Volo " + sigla
                + " " + aereoportoPartenza.getCitta() + " " + aereoportoPartenza.getNome()
                + " - " + aereoportoArrivo.getCitta() + " " + aereoportoArrivo.getNome());
    }

    public void printPasseggeri() {
        for (int i = 0; i < numeroPasseggeri; i++) {
            System.out.println("Passeggero " + (i + 1) + ": " + elencoPasseggeri[i].getNome());
        }
    }

    public void printPasseggeriVeg() {
        for (int i = 0; i < numeroPasseggeri; i++) {
            if (elencoPasseggeri[i].getPastoRichiesto() == "Vegetariano")
                System.out.println("Passeggero con pasto vegetariano " + (i + 1) + ": " + elencoPasseggeri[i].getNome());
        }
    }
}
