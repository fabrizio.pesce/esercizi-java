package it.aereoporto;

public class MainClass{
    public static void main(String[] args) {
        Aereoporto a1 = new Aereoporto("Galilei", "Pisa", "PSA");
        Aereoporto a2 = new Aereoporto("Fiumicino", "Roma", "RMA");
        System.out.println(a1.getNome());
        Passeggero p = new Passeggero("Fabrizio", "IT", "AZ124", "16F");
        Passeggero p2 = new Passeggero("Pablo", "IT", "AZ124", "16F", "Vegetariano");
        Volo v = new Volo("AZ124", a1, a2, "Airbus300", 10);
        v.addPassenger(p);
        v.addPassenger(p2);
        v.printVolo();
        v.printPasseggeri();
        v.printPasseggeriVeg();
        Aereoporto a3 = new Aereoporto("Borgo Panigale", "Bologna", "BLQ");
        Aereoporto a4 = new Aereoporto("Villafranca", "Verona", "VER");
        Volo scalo = new Volo("AZ122", a3, a4, "Airbus300", 10);
        VoloNonDiretto vn = new VoloNonDiretto("AZ127", a1, a2, "Airbus300", 10,3);
        vn.aggiungiScalo(scalo);
        vn.printVolo();
    }
}

