package it.ArrayList;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MainClass {
    public static void main(String[] args) {
        List<Studente> listaStudenti = new ArrayList<>();
        Studente s1 = new Studente("Andrea", 8);
        Studente s2 = new Studente("Fabrizio", 7);

        listaStudenti.add(s1);
        listaStudenti.add(s2);

        System.out.println(MainClass.calcolaMedia(listaStudenti));

    }

    public static double calcolaMedia(List<Studente> listaStudenti){
        Iterator iterator = listaStudenti.iterator();
        int total = 0;
        while(iterator.hasNext()){
            total += (int) iterator.next();
        }
        return (double) total /listaStudenti.size();
    }
}
