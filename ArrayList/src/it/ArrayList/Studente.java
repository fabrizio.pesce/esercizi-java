package it.ArrayList;

public class Studente {
    String nome;
    int voto;

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setVoto(int voto) {
        this.voto = voto;
    }

    public String getNome() {
        return nome;
    }

    public int getVoto() {
        return voto;
    }

    public Studente(String nome, int voto) {
        this.nome = nome;
        this.voto = voto;
    }
}
