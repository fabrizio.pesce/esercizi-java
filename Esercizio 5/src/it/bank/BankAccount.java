package it.bank;
/*
    Scrivere un programma che costruisca un conto bancario,
    versi in esso $1000, prelevi da esso $500, prelevi altri $400
    e infine visualizzi il saldo rimanente
 */
public class BankAccount {
    private int amount;
    public BankAccount(){
    }

    public void setAmount(int amount){
        this.amount = amount;
    }

    public int getAmount(){
        return this.amount;
    }

    public void withdrawal(int amount){
        setAmount(this.amount-amount);
    }

    public void deposit(int amount){
        setAmount(this.amount+amount);
    }

    public void printStatement(){
        System.out.println("Sul conto sono presenti attualmente: "+amount+"€");
    }

}
