package it.bank;

public class MainClass {
    public static void main(String[] args) {
        BankAccount ba = new BankAccount();
        ba.deposit(1000);
        ba.withdrawal(500);
        ba.withdrawal(400);
        ba.printStatement();

    }
}
