package it.esercizio;

public class Esercizio {
    public static void main(String[] args) {
        String s = "Casa";
        char c = 'a';
        System.out.println("Esempio di utilizzo di funzioni: Stringa \""+s+"\" e carattere '"+c+"'");
        System.out.println("Funzione per conoscere se c'è un'occorrenza: "+inString(c, s));
        System.out.println("Funzione per conoscere quante volte c'è stata l'occorrenza: "+occurrencesInString(c, s));
        System.out.println("Funzione per conoscere l'indice della prima occorrenza: "+indexOfFirstOccurrence(c, s));

    }
    public static boolean inString(char c, String s){
        for(int i = 0; i<s.length(); i++){
            if(s.toLowerCase().charAt(i) == Character.toLowerCase(c)){
                return true;
            }
        }
        return false;
    }
    public static int occurrencesInString(char c, String s){
        int occurrences = 0;
        for(int i = 0; i<s.length(); i++){
            if(s.toLowerCase().charAt(i) == Character.toLowerCase(c)){
                occurrences++;
            }
        }

        return occurrences;
    }

    /**
     * Author Fabrizio Pesce
     * @param c
     * @param s
     * @return
     */
    public static int indexOfFirstOccurrence(char c, String s){
        for(int i = 0; i<s.length(); i++){
            if(s.toLowerCase().charAt(i) == Character.toLowerCase(c)){
                return i;
            }
        }
        return -1;
    }
}
