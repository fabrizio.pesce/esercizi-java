package it.school;

public class MainClass {
    public static void main(String[] args) {
        StudenteInformatica si = new StudenteInformatica("Andrea", 186397, "Informatica - programmazione");
        StudenteMatematica sm = new StudenteMatematica("Fabrizio", 186398, "Matematica");

        si.stampaInfo();
        sm.stampaInfo();
    }
}
