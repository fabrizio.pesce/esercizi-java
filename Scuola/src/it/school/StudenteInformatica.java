package it.school;

public class StudenteInformatica extends Studente{
    String corsoDiStudi;
    public StudenteInformatica(String nome, int matricola, String corsoDiStudi) {
        super(nome, matricola);
        this.corsoDiStudi = corsoDiStudi;
    }
    @Override
    public void stampaInfo() {
        super.stampaInfo();
        System.out.println("Corso di studi: "+this.corsoDiStudi);
    }
}
