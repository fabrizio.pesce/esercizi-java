package it.Istruzione;

import java.util.ArrayList;
import java.util.List;

public abstract class Persona {
    private String nome;
    private String cognome;
    private int idUnivoco;
    private List<Corso> listaCorsi;

    public Persona(String nome, String cognome, int idUnivoco) {
        this.nome = nome;
        this.cognome = cognome;
        this.idUnivoco = idUnivoco;
        listaCorsi = new ArrayList<>();
    }

    public String getNome() {
        return nome;
    }

    public String getCognome() {
        return cognome;
    }

    public int getIdUnivoco() {
        return idUnivoco;
    }

    public List<Corso> getListaCorsi() {
        return listaCorsi;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public void setIdUnivoco(int idUnivoco) {
        this.idUnivoco = idUnivoco;
    }

    public void setListaCorsi(List<Corso> listaCorsi) {
        this.listaCorsi = listaCorsi;
    }
}
