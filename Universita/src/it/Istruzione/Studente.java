package it.Istruzione;

import java.util.ArrayList;
import java.util.List;

public class Studente extends Persona{
    private int annoImmatricolazione;
    public Studente(String nome, String cognome, int matricola, int annoImmatricolazione) {
        super(nome, cognome, matricola);
        this.annoImmatricolazione = annoImmatricolazione;
    }

    public int getAnnoImmatricolazione() {
        return annoImmatricolazione;
    }
    public void setAnnoImmatricolazione(int annoImmatricolazione) {
        this.annoImmatricolazione = annoImmatricolazione;
    }

    public void aggiungiCorso(Corso nuovoCorso) throws IscrizioniException {
        if(nuovoCorso.isStudentSubscribed(this)){
            throw new IscrizioniException("Studente già iscritto");
        }
        if(nuovoCorso.getListaStudenti().size() < nuovoCorso.getMaxStudenti()){
            List<Corso> listaCorsiAggiornata = this.getListaCorsi();
            nuovoCorso.addStudent(this);
            listaCorsiAggiornata.add(nuovoCorso);
            this.setListaCorsi(listaCorsiAggiornata);
        }else{
            throw new IscrizioniException("Corso pieno");
        }
    }
    public void rimuoviCorso(Corso nuovoCorso) {
        List<Corso> listaCorsiAggiornata = this.getListaCorsi();
        listaCorsiAggiornata.remove(nuovoCorso);
        if(nuovoCorso.isStudentSubscribed(this)){
            nuovoCorso.removeStudent(this);
        }
        this.setListaCorsi(listaCorsiAggiornata);
    }
}
