package it.Istruzione;

import java.util.ArrayList;
import java.util.List;

public class Corso {
    private String titoloCorso;
    private int codiceCorso;
    private Professore professoreTitolare;
    private List<Studente> listaStudenti;
    private int maxStudenti;

    public Corso(String titoloCorso, int codiceCorso, Professore professoreTitolare, int maxStudenti) {
        this.titoloCorso = titoloCorso;
        this.codiceCorso = codiceCorso;
        this.professoreTitolare = professoreTitolare;
        professoreTitolare.aggiungiCorso(this);
        this.maxStudenti = maxStudenti;
        listaStudenti = new ArrayList<>(maxStudenti);
    }

    public String getTitoloCorso() {
        return titoloCorso;
    }

    public int getCodiceCorso() {
        return codiceCorso;
    }

    public Professore getProfessoreTitolare() {
        return professoreTitolare;
    }

    public List<Studente> getListaStudenti() {
        return listaStudenti;
    }

    public int getMaxStudenti() {
        return maxStudenti;
    }

    public void setTitoloCorso(String titoloCorso) {
        this.titoloCorso = titoloCorso;
    }

    public void setCodiceCorso(int codiceCorso) {
        this.codiceCorso = codiceCorso;
    }

    public void setProfessoreTitolare(Professore professoreTitolare) {
        this.professoreTitolare = professoreTitolare;
    }

    public void setListaStudenti(List<Studente> listaStudenti) {
        this.listaStudenti = listaStudenti;
    }

    public void setMaxStudenti(int maxStudenti) {
        this.maxStudenti = maxStudenti;
    }

    public void addStudent(Studente studente) throws IscrizioniException {
        if(this.listaStudenti.size() < this.maxStudenti)
            this.listaStudenti.add(studente);
        else
            throw new IscrizioniException("Corso pieno");
    }

    public boolean isStudentSubscribed(Studente studenteDaCercare){
        for(Studente studente : listaStudenti){
            if(studenteDaCercare == studente){
                return true;
            }
        }
        return false;
    }
    public void removeStudent(Studente studente){
        this.listaStudenti.remove(studente);
    }

}
