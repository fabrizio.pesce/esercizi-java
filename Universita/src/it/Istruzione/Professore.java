package it.Istruzione;

import java.util.ArrayList;
import java.util.List;

public class Professore extends Persona{
    public Professore(String nome, String cognome, int idUnivoco) {
        super(nome, cognome, idUnivoco);

    }
    public void aggiungiCorso(Corso nuovoCorso) {
        nuovoCorso.setProfessoreTitolare(this);
        List<Corso> listaCorsiAggiornata = this.getListaCorsi();
        listaCorsiAggiornata.add(nuovoCorso);
        this.setListaCorsi(listaCorsiAggiornata);
    }
    public void rimuoviCorso(Corso nuovoCorso) {
        List<Corso> listaCorsiAggiornata = this.getListaCorsi();
        listaCorsiAggiornata.remove(nuovoCorso);
        nuovoCorso.setProfessoreTitolare(null);
        this.setListaCorsi(listaCorsiAggiornata);
    }
}
