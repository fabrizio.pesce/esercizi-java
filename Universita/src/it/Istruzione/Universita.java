package it.Istruzione;

import java.util.ArrayList;
import java.util.List;

public class Universita {
    private List<Studente> listaStudenti;
    private List<Professore> listaProfessori;
    private List<Corso> corsiErogati;

    public Universita(){
        this.listaStudenti = new ArrayList<>();
        this.listaProfessori = new ArrayList<>();
        this.corsiErogati = new ArrayList<>();
    }

    public void addPersona(Persona persona){
        if(persona instanceof Studente){
            addStudent((Studente)persona);
        }else{
            addProfessore((Professore)persona);
        }
    }

    public void addStudent(Studente studente){
        this.listaStudenti.add(studente);
    }
    public void addProfessore(Professore professore){
        this.listaProfessori.add(professore);
    }

    public void addCorso(Corso corso){
        this.corsiErogati.add(corso);
    }

    public void addStudenteInCorso(Studente studente, Corso corsoScelto) throws IscrizioniException {
        for(Corso corso : this.corsiErogati){
            if(corso == corsoScelto){
                studente.aggiungiCorso(corso);
                return;
            }
        }
    }

    public void addProfessoreInCorso(Professore professore, Corso corsoScelto){
        for(Corso corso : this.corsiErogati){
            if(corso == corsoScelto){
                professore.aggiungiCorso(corso);
                return;
            }
        }
    }

    public void printCorsi(Persona persona){
        for(Corso corso : persona.getListaCorsi()){
            System.out.println("Corso: "+corso.getTitoloCorso());
        }
    }
}
