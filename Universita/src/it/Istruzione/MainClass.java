package it.Istruzione;

public class MainClass {
    public static void main(String[] args) {
        Studente studente1 = new Studente("Pippo", "Palla", 1, 2020);
        Studente studente2 = new Studente("Fabrizio", "Pesce", 2, 2021);

        Professore professore = new Professore("Paolo", "Buso", 3);

        Corso corso1 = new Corso("Dinamica dei fluidi", 1, professore, 1);
        Corso corso2 = new Corso("Meccanica", 1, professore, 1);

        Universita universita = new Universita();

        universita.addPersona(professore);
        universita.addPersona(studente1);
        universita.addPersona(studente2);
        universita.addCorso(corso1);
        universita.addCorso(corso2);

        try {
            universita.addStudenteInCorso(studente1, corso1);
            universita.addStudenteInCorso(studente1, corso2);
        } catch (IscrizioniException iscrizioniException) {
            System.out.println(iscrizioniException.getMessage());
        }
        universita.addProfessoreInCorso(professore, corso1);
        universita.printCorsi(studente1);

    }
}
