package it.Istruzione;

public class IscrizioniException extends Exception{
    public IscrizioniException() {
    }

    public IscrizioniException(String message) {
        super(message);
    }
}
