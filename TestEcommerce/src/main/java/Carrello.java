import java.util.HashMap;
import java.util.Map;

public class Carrello {
    private HashMap<Prodotto, Integer> prodottiNelCarrello;
    private Promozione promozione;

    public Carrello() {
        this.prodottiNelCarrello = new HashMap<>();
        this.promozione = null;
    }

    public Carrello(HashMap<Prodotto, Integer> prodottiDaOrdinare){
        this.prodottiNelCarrello = prodottiDaOrdinare;
        this.promozione = null;
    }

    public HashMap<Prodotto, Integer> getProdottiNelCarrello() {
        return prodottiNelCarrello;
    }

    public Promozione getPromozione() {
        return promozione;
    }

    public void setProdottiNelCarrello(HashMap<Prodotto, Integer> prodottiNelCarrello) {
        this.prodottiNelCarrello = prodottiNelCarrello;
    }

    public void setPromozione(Promozione promozione) {
        this.promozione = promozione;
    }

    public void addNewProductToCart(Prodotto prodotto, int quantita){
        this.prodottiNelCarrello.put(prodotto, quantita);
    }
    public void removeProductFromCart(Prodotto prodotto){
        this.prodottiNelCarrello.remove(prodotto);
    }

    public void addPromo(Promozione promozione){
        this.promozione = promozione;
    }

    public double calcTotal(){
        double total = 0;
        for(Map.Entry<Prodotto, Integer> entry : this.prodottiNelCarrello.entrySet()){
            total += ((entry.getKey().getPrezzo())* entry.getValue());
        }
        System.out.println("Prezzo iniziale: "+total);
        System.out.println("Prezzo con promozione: "+total*(1-promozione.getPercentualeSconto()));
        return (Math.round(total*100D))/100D;
    }
    public double calcTotalWithPromotion(){
        double total = 0;
        for(Map.Entry<Prodotto, Integer> entry : this.prodottiNelCarrello.entrySet()){
            total += ((entry.getKey().getPrezzo())* entry.getValue());
        }
        System.out.println("Prezzo iniziale: "+total);
        System.out.println("Prezzo con promozione: "+total*(1-promozione.getPercentualeSconto()));
        return (Math.round(total*(1-promozione.getPercentualeSconto())*100D))/100D;
    }
}
