public class Promozione {
    private String codicePromozionale;
    private String descrizione;
    private double percentualeSconto;

    public Promozione(String codicePromozionale, String descrizione, double percentualeSconto) {
        this.codicePromozionale = codicePromozionale;
        this.descrizione = descrizione;
        this.percentualeSconto = percentualeSconto;
    }

    public String getCodicePromozionale() {
        return codicePromozionale;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public double getPercentualeSconto() {
        return percentualeSconto;
    }

    public void setCodicePromozionale(String codicePromozionale) {
        this.codicePromozionale = codicePromozionale;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public void setPercentualeSconto(double percentualeSconto) {
        this.percentualeSconto = percentualeSconto;
    }

    public Ordine addDiscountToOrder(Ordine order){
        order.setTotaleOrdine(order.getTotaleOrdine()*(1-this.percentualeSconto));
        return order;
    }
}
