import java.util.HashMap;
import java.util.Map;

public class GestioneInventario {
    private HashMap<Prodotto, Integer> prodotti;

    public GestioneInventario() {
        this.prodotti = new HashMap<>();
    }

    public HashMap<Prodotto, Integer> getProdotti() {
        return prodotti;
    }

    public void setProdotti(HashMap<Prodotto, Integer> prodotti) {
        this.prodotti = prodotti;
    }

    public void updateProduct(Prodotto prodotto, int quantita){
        this.prodotti.put(prodotto, quantita);
    }

    public boolean isAvailable(Prodotto prodotto){
        return this.prodotti.get(prodotto)>0;
    }

    public void supplyOrder(HashMap<Prodotto, Integer> ordineDiRifornimento){
        this.prodotti.putAll(ordineDiRifornimento);
    }
}
