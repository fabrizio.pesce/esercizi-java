import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Ordine {
    private String idOrdine;
    private HashMap<Prodotto, Integer> prodottiOrdinati;
    public enum Stato{
        IN_ATTESA,
        SPEDITO,
        CONSEGNATO
    }

    private Stato statoOrdine;
    private double totaleOrdine;

    public Ordine(){
        this.prodottiOrdinati = new HashMap<>();
    }

    public Ordine(HashMap<Prodotto, Integer> prodottiDaOrdinare){
        this.prodottiOrdinati = prodottiDaOrdinare;
    }

    public String getIdOrdine() {
        return idOrdine;
    }

    public Stato getStatoOrdine() {
        return statoOrdine;
    }

    public double getTotaleOrdine() {
        return totaleOrdine;
    }

    public void setIdOrdine(String idOrdine) {
        this.idOrdine = idOrdine;
    }

    public void setStatoOrdine(Stato statoOrdine) {
        this.statoOrdine = statoOrdine;
    }

    public void setTotaleOrdine(double totaleOrdine) {
        this.totaleOrdine = (Math.round(totaleOrdine*100D))/100D;
    }

    public void addItemToOrder(Prodotto prodotto, int quantita){
        this.prodottiOrdinati.put(prodotto, quantita);
        orderTotal();
    }
    public void removeItemFromOrder(Prodotto prodotto){
        this.prodottiOrdinati.remove(prodotto);
        orderTotal();
    }
    public void orderTotal(){
        double total = 0;
        for(Map.Entry<Prodotto, Integer> entry : this.prodottiOrdinati.entrySet()){
            total += ((entry.getKey().getPrezzo())* entry.getValue());
        }

        this.totaleOrdine = (Math.round(total*100D))/100D;
    }
}
