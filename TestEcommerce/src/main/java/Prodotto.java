public class Prodotto {
    private String idProdotto;
    private String nome;
    private double prezzo;
    private int quantitaInventario;

    public Prodotto(String idProdotto, String nome, double prezzo, int quantitaInventario) {
        this.idProdotto = idProdotto;
        this.nome = nome;
        this.prezzo = prezzo;
        this.quantitaInventario = quantitaInventario;
    }

    public String getIdProdotto() {
        return idProdotto;
    }

    public String getNome() {
        return nome;
    }

    public double getPrezzo() {
        return prezzo;
    }

    public int getQuantitaInventario() {
        return quantitaInventario;
    }

    public void setIdProdotto(String idProdotto) {
        this.idProdotto = idProdotto;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setPrezzo(double prezzo) {
        this.prezzo = prezzo;
    }

    public void setQuantitaInventario(int quantitaInventario) {
        this.quantitaInventario = quantitaInventario;
    }

    public boolean isInInventory(){
        return getQuantitaInventario() > 0;
    }
}
