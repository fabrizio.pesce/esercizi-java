import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class OrdineTest {

    Ordine ordine;
    static Prodotto prodotto1;
    static Prodotto prodotto2;

    @BeforeAll
    static void setUpProduct(){
        prodotto1 = new Prodotto("1", "Maglietta bianca", 10.99, 10);
        prodotto2 = new Prodotto("2", "Maglietta blu", 10.99, 8);
    }

    @BeforeEach
    void setUp(){
        ordine = new Ordine();
    }

    @Test
    void testAddRemoveProduct(){
        ordine.addItemToOrder(prodotto1, 2);
        assertEquals(21.98, ordine.getTotaleOrdine());
        ordine.removeItemFromOrder(prodotto1);
        assertEquals(0, ordine.getTotaleOrdine());
        ordine.setStatoOrdine(Ordine.Stato.SPEDITO);
        assertEquals(Ordine.Stato.SPEDITO, ordine.getStatoOrdine());
        HashMap<Prodotto, Integer> listaProdotti = new HashMap<>();
        listaProdotti.put(prodotto1, prodotto1.getQuantitaInventario());
        Ordine ordine2 = new Ordine(listaProdotti);
        assertNotNull(ordine2);
    }

}