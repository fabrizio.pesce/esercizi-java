import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PromozioneTest {

    Promozione promozione;
    Ordine ordine;
    static Prodotto prodotto1;
    static Prodotto prodotto2;

    @BeforeAll
    static void setUpProduct(){
        prodotto1 = new Prodotto("1", "Maglietta bianca", 10.99, 10);
        prodotto2 = new Prodotto("2", "Maglietta blu", 10.99, 8);
    }

    @BeforeEach
    void setUp(){
        ordine = new Ordine();
        promozione = new Promozione("BUBU20", "Sconto del 20%", 0.20);
    }

    @Test
    void testAddRemoveProduct(){
        ordine.addItemToOrder(prodotto1, 2);
        assertEquals(21.98, ordine.getTotaleOrdine());
        ordine = promozione.addDiscountToOrder(ordine);
        assertEquals(17.58, ordine.getTotaleOrdine());
        ordine.removeItemFromOrder(prodotto1);
        assertEquals(0, ordine.getTotaleOrdine());
        ordine.addItemToOrder(prodotto1, 2);
        promozione.setCodicePromozionale("BUBU30");
        promozione.setDescrizione("Codice sconto del 30%");
        promozione.setPercentualeSconto(0.3);
        ordine = promozione.addDiscountToOrder(ordine);
        assertEquals(15.39, ordine.getTotaleOrdine());

    }

}