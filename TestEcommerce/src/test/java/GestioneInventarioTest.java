import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class GestioneInventarioTest {
    GestioneInventario gestioneInventario;
    static Prodotto prodotto1;
    static Prodotto prodotto2;

    @BeforeAll
    static void setUpProduct(){
        prodotto1 = new Prodotto("1", "Maglietta bianca", 10.99, 30);
        prodotto2 = new Prodotto("2", "Maglietta blu", 10.99, 20);
    }

    @BeforeEach
    void setUp(){
        gestioneInventario = new GestioneInventario();
    }

    @Test
    void testQuantitaRifornimenti(){
        gestioneInventario.updateProduct(prodotto1,prodotto1.getQuantitaInventario());
        gestioneInventario.updateProduct(prodotto2,prodotto2.getQuantitaInventario());
        assertEquals(2, gestioneInventario.getProdotti().size());
        assertEquals(30, gestioneInventario.getProdotti().get(prodotto1));
        assertEquals(20, gestioneInventario.getProdotti().get(prodotto2));
        Prodotto prodotto3 = new Prodotto("3", "Calzini a pois", 3.99, 4);
        HashMap<Prodotto, Integer> supply = new HashMap<>();
        supply.put(prodotto3, 4);
        gestioneInventario.supplyOrder(supply);
        assertEquals(3, gestioneInventario.getProdotti().size());

    }
    @Test
    void testVerificaDisponibilita(){
        //Controllo che il prodotto sia in inventario, poi lo inserisco
        assertTrue(prodotto1.isInInventory());
        assertTrue(prodotto2.isInInventory());
        gestioneInventario.updateProduct(prodotto1,prodotto1.getQuantitaInventario());
        gestioneInventario.updateProduct(prodotto2,prodotto1.getQuantitaInventario());
        assertTrue(gestioneInventario.isAvailable(prodotto1));
        assertTrue(gestioneInventario.isAvailable(prodotto2));
        prodotto1.setQuantitaInventario(0);
        gestioneInventario.updateProduct(prodotto1,prodotto1.getQuantitaInventario());
        assertFalse(gestioneInventario.isAvailable(prodotto1));
    }

}