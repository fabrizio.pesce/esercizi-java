import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CarrelloTest {
    Carrello carrello;
    Promozione promozione;
    static Prodotto prodotto1;
    static Prodotto prodotto2;

    @BeforeAll
    static void setUpProduct(){
        prodotto1 = new Prodotto("1", "Maglietta bianca", 10.99, 10);
        prodotto2 = new Prodotto("2", "Maglietta blu", 10.99, 8);
    }

    @BeforeEach
    void setUp(){
        carrello = new Carrello();
        promozione = new Promozione("BUBU20", "Sconto del 20%", 0.20);
    }

    @Test
    void testAddRemoveProduct(){
        assertNotNull(carrello);
        carrello.addNewProductToCart(prodotto1, 2);
        carrello.addNewProductToCart(prodotto2, 1);
        assertEquals(2, carrello.getProdottiNelCarrello().size());
        carrello.addPromo(promozione);
        carrello.removeProductFromCart(prodotto2);
        assertEquals(1, carrello.getProdottiNelCarrello().size());
        assertEquals(21.98, carrello.calcTotal());
        assertEquals(17.58, carrello.calcTotalWithPromotion());
    }

}